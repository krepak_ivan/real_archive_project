package kz.aitu.real_archive_project.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "case_table")
public class CaseTable {
    @Id
    private long id;
    private String case_number;
    private String folder_number;
    private String case_title_ru;
    private String case_title_kz;
    private String case_title_en;
    private long start_date;
    private long end_date;
    private long pages_amount;
    private boolean ecp_flag;
    private String ecp_text;
    private boolean naf_send;
    private boolean deletion_flag;
    private boolean restricted_access_flag;
    private String hash;
    private int version;
    private String id_version;
    private boolean is_active_flag;
    private String comment;
    private long id_location;
    private long id_index;
    private long id_deletion_document;
    private long structure_unit_id;
    private String blockchain_address;
    private long adding_to_blockchain_date;
    private long creating_date;
    private long case_author;
    private long case_editing;
    private long case_editing_author;

    public CaseTable(long id, String case_number, String folder_number, String case_title_ru, String case_title_kz,
                     String case_title_en, long start_date, long end_date, long pages_amount, boolean ecp_flag,
                     String ecp_text, boolean naf_send, boolean deletion_flag, boolean restricted_access_flag,
                     String hash, int version, String id_version, boolean is_active_flag, String comment,
                     long id_location, long id_index, long id_deletion_document, long structure_unit_id,
                     String blockchain_address, long adding_to_blockchain_date, long creating_date, long case_author,
                     long case_editing, long case_editing_author) {
        this.id = id;
        this.case_number = case_number;
        this.folder_number = folder_number;
        this.case_title_ru = case_title_ru;
        this.case_title_kz = case_title_kz;
        this.case_title_en = case_title_en;
        this.start_date = start_date;
        this.end_date = end_date;
        this.pages_amount = pages_amount;
        this.ecp_flag = ecp_flag;
        this.ecp_text = ecp_text;
        this.naf_send = naf_send;
        this.deletion_flag = deletion_flag;
        this.restricted_access_flag = restricted_access_flag;
        this.hash = hash;
        this.version = version;
        this.id_version = id_version;
        this.is_active_flag = is_active_flag;
        this.comment = comment;
        this.id_location = id_location;
        this.id_index = id_index;
        this.id_deletion_document = id_deletion_document;
        this.structure_unit_id = structure_unit_id;
        this.blockchain_address = blockchain_address;
        this.adding_to_blockchain_date = adding_to_blockchain_date;
        this.creating_date = creating_date;
        this.case_author = case_author;
        this.case_editing = case_editing;
        this.case_editing_author = case_editing_author;
    }

    public CaseTable() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCase_number() {
        return case_number;
    }

    public void setCase_number(String case_number) {
        this.case_number = case_number;
    }

    public String getFolder_number() {
        return folder_number;
    }

    public void setFolder_number(String folder_number) {
        this.folder_number = folder_number;
    }

    public String getCase_title_ru() {
        return case_title_ru;
    }

    public void setCase_title_ru(String case_title_ru) {
        this.case_title_ru = case_title_ru;
    }

    public String getCase_title_kz() {
        return case_title_kz;
    }

    public void setCase_title_kz(String case_title_kz) {
        this.case_title_kz = case_title_kz;
    }

    public String getCase_title_en() {
        return case_title_en;
    }

    public void setCase_title_en(String case_title_en) {
        this.case_title_en = case_title_en;
    }

    public long getStart_date() {
        return start_date;
    }

    public void setStart_date(long start_date) {
        this.start_date = start_date;
    }

    public long getEnd_date() {
        return end_date;
    }

    public void setEnd_date(long end_date) {
        this.end_date = end_date;
    }

    public long getPages_amount() {
        return pages_amount;
    }

    public void setPages_amount(long pages_amount) {
        this.pages_amount = pages_amount;
    }

    public boolean isEcp_flag() {
        return ecp_flag;
    }

    public void setEcp_flag(boolean ecp_flag) {
        this.ecp_flag = ecp_flag;
    }

    public String getEcp_text() {
        return ecp_text;
    }

    public void setEcp_text(String ecp_text) {
        this.ecp_text = ecp_text;
    }

    public boolean isNaf_send() {
        return naf_send;
    }

    public void setNaf_send(boolean naf_send) {
        this.naf_send = naf_send;
    }

    public boolean isDeletion_flag() {
        return deletion_flag;
    }

    public void setDeletion_flag(boolean deletion_flag) {
        this.deletion_flag = deletion_flag;
    }

    public boolean isRestricted_access_flag() {
        return restricted_access_flag;
    }

    public void setRestricted_access_flag(boolean restricted_access_flag) {
        this.restricted_access_flag = restricted_access_flag;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getId_version() {
        return id_version;
    }

    public void setId_version(String id_version) {
        this.id_version = id_version;
    }

    public boolean isIs_active_flag() {
        return is_active_flag;
    }

    public void setIs_active_flag(boolean is_active_flag) {
        this.is_active_flag = is_active_flag;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getId_location() {
        return id_location;
    }

    public void setId_location(long id_location) {
        this.id_location = id_location;
    }

    public long getId_index() {
        return id_index;
    }

    public void setId_index(long id_index) {
        this.id_index = id_index;
    }

    public long getId_deletion_document() {
        return id_deletion_document;
    }

    public void setId_deletion_document(long id_deletion_document) {
        this.id_deletion_document = id_deletion_document;
    }

    public long getStructure_unit_id() {
        return structure_unit_id;
    }

    public void setStructure_unit_id(long structure_unit_id) {
        this.structure_unit_id = structure_unit_id;
    }

    public String getBlockchain_address() {
        return blockchain_address;
    }

    public void setBlockchain_address(String blockchain_address) {
        this.blockchain_address = blockchain_address;
    }

    public long getAdding_to_blockchain_date() {
        return adding_to_blockchain_date;
    }

    public void setAdding_to_blockchain_date(long adding_to_blockchain_date) {
        this.adding_to_blockchain_date = adding_to_blockchain_date;
    }

    public long getCreating_date() {
        return creating_date;
    }

    public void setCreating_date(long creating_date) {
        this.creating_date = creating_date;
    }

    public long getCase_author() {
        return case_author;
    }

    public void setCase_author(long case_author) {
        this.case_author = case_author;
    }

    public long getCase_editing() {
        return case_editing;
    }

    public void setCase_editing(long case_editing) {
        this.case_editing = case_editing;
    }

    public long getCase_editing_author() {
        return case_editing_author;
    }

    public void setCase_editing_author(long case_editing_author) {
        this.case_editing_author = case_editing_author;
    }
}