package kz.aitu.real_archive_project.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "delete_document")
public class DeleteDocument {
    @Id
    private long id;
    private String document_number;
    private String reason;
    private long company_unit_id;
    private long creation_date;
    private long author;
    private long edition_date;
    private long edition_author;

    public DeleteDocument(long id, String document_number, String reason, long company_unit_id, long creation_date,
                          long author, long edition_date, long edition_author) {
        this.id = id;
        this.document_number = document_number;
        this.reason = reason;
        this.company_unit_id = company_unit_id;
        this.creation_date = creation_date;
        this.author = author;
        this.edition_date = edition_date;
        this.edition_author = edition_author;
    }

    public DeleteDocument() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDocument_number() {
        return document_number;
    }

    public void setDocument_number(String document_number) {
        this.document_number = document_number;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public long getCompany_unit_id() {
        return company_unit_id;
    }

    public void setCompany_unit_id(long company_unit_id) {
        this.company_unit_id = company_unit_id;
    }

    public long getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(long creation_date) {
        this.creation_date = creation_date;
    }

    public long getAuthor() {
        return author;
    }

    public void setAuthor(long author) {
        this.author = author;
    }

    public long getEdition_date() {
        return edition_date;
    }

    public void setEdition_date(long edition_date) {
        this.edition_date = edition_date;
    }

    public long getEdition_author() {
        return edition_author;
    }

    public void setEdition_author(long edition_author) {
        this.edition_author = edition_author;
    }
}