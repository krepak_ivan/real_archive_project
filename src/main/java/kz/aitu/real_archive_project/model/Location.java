package kz.aitu.real_archive_project.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "location")
public class Location {
    @Id
    private long id;
    private String row;
    private String line;
    private String column_location;
    private String box;
    private long company_unit_id;
    private long created_timestamp;
    private long created_by;
    private long updated_timestamp;
    private long updated_by;

    public Location(long id, String row, String line, String column_location, String box, long company_unit_id,
                    long created_timestamp, long created_by, long updated_timestamp, long updated_by) {
        this.id = id;
        this.row = row;
        this.line = line;
        this.column_location = column_location;
        this.box = box;
        this.company_unit_id = company_unit_id;
        this.created_timestamp = created_timestamp;
        this.created_by = created_by;
        this.updated_timestamp = updated_timestamp;
        this.updated_by = updated_by;
    }

    public Location() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getColumn_location() {
        return column_location;
    }

    public void setColumn_location(String column_location) {
        this.column_location = column_location;
    }

    public String getBox() {
        return box;
    }

    public void setBox(String box) {
        this.box = box;
    }

    public long getCompany_unit_id() {
        return company_unit_id;
    }

    public void setCompany_unit_id(long company_unit_id) {
        this.company_unit_id = company_unit_id;
    }

    public long getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(long created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public long getCreated_by() {
        return created_by;
    }

    public void setCreated_by(long created_by) {
        this.created_by = created_by;
    }

    public long getUpdated_timestamp() {
        return updated_timestamp;
    }

    public void setUpdated_timestamp(long updated_timestamp) {
        this.updated_timestamp = updated_timestamp;
    }

    public long getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(long updated_by) {
        this.updated_by = updated_by;
    }
}