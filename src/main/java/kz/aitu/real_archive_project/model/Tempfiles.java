package kz.aitu.real_archive_project.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tempfiles")
public class Tempfiles {
    @Id
    private long id;
    private String file_binary;
    private String file_binary_byte;

    public Tempfiles() {}

    public Tempfiles(long id, String file_binary, String file_binary_byte) {
        this.id = id;
        this.file_binary = file_binary;
        this.file_binary_byte = file_binary_byte;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFile_binary() {
        return file_binary;
    }

    public void setFile_binary(String file_binary) {
        this.file_binary = file_binary;
    }

    public String getFile_binary_byte() {
        return file_binary_byte;
    }

    public void setFile_binary_byte(String file_binary_byte) {
        this.file_binary_byte = file_binary_byte;
    }
}