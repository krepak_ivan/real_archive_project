package kz.aitu.real_archive_project.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "company")
public class Company {
    @Id
    private long id;
    private String name_ru;
    private String name_kz;
    private String name_en;
    private String bin;
    private long parent_id;
    private long fond_id;
    private long created_timestamp;
    private long created_by;
    private long updated_timestamp;
    private long updated_by;

    public Company(long id, String name_ru, String name_kz, String name_en, String bin, long parent_id, long fond_id,
                   long created_timestamp, long created_by, long updated_timestamp, long updated_by) {
        this.id = id;
        this.name_ru = name_ru;
        this.name_kz = name_kz;
        this.name_en = name_en;
        this.bin = bin;
        this.parent_id = parent_id;
        this.fond_id = fond_id;
        this.created_timestamp = created_timestamp;
        this.created_by = created_by;
        this.updated_timestamp = updated_timestamp;
        this.updated_by = updated_by;
    }

    public Company() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName_ru() {
        return name_ru;
    }

    public void setName_ru(String name_ru) {
        this.name_ru = name_ru;
    }

    public String getName_kz() {
        return name_kz;
    }

    public void setName_kz(String name_kz) {
        this.name_kz = name_kz;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public long getParent_id() {
        return parent_id;
    }

    public void setParent_id(long parent_id) {
        this.parent_id = parent_id;
    }

    public long getFond_id() {
        return fond_id;
    }

    public void setFond_id(long fond_id) {
        this.fond_id = fond_id;
    }

    public long getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(long created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public long getCreated_by() {
        return created_by;
    }

    public void setCreated_by(long created_by) {
        this.created_by = created_by;
    }

    public long getUpdated_timestamp() {
        return updated_timestamp;
    }

    public void setUpdated_timestamp(long updated_timestamp) {
        this.updated_timestamp = updated_timestamp;
    }

    public long getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(long updated_by) {
        this.updated_by = updated_by;
    }
}