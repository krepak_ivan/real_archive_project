package kz.aitu.real_archive_project.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "request_table")
public class RequestTable {
    @Id
    private long id;
    private long request_user_id;
    private long response_user_id;
    private long case_index_id;
    private String created_type;
    private String comment;
    private String status;
    private long timestamp;
    private long sharetimestamp;
    private long sharefinish;
    private boolean favorite;
    private long update_timestamp;
    private long update_by;
    private String declinenote;
    private long company_unit_id;
    private long from_request_id;

    public RequestTable(long id, long request_user_id, long response_user_id, long case_index_id, String created_type,
                        String comment, String status, long timestamp, long sharetimestamp, long sharefinish,
                        boolean favorite, long update_timestamp, long update_by, String declinenote,
                        long company_unit_id, long from_request_id) {
        this.id = id;
        this.request_user_id = request_user_id;
        this.response_user_id = response_user_id;
        this.case_index_id = case_index_id;
        this.created_type = created_type;
        this.comment = comment;
        this.status = status;
        this.timestamp = timestamp;
        this.sharetimestamp = sharetimestamp;
        this.sharefinish = sharefinish;
        this.favorite = favorite;
        this.update_timestamp = update_timestamp;
        this.update_by = update_by;
        this.declinenote = declinenote;
        this.company_unit_id = company_unit_id;
        this.from_request_id = from_request_id;
    }

    public RequestTable() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRequest_user_id() {
        return request_user_id;
    }

    public void setRequest_user_id(long request_user_id) {
        this.request_user_id = request_user_id;
    }

    public long getResponse_user_id() {
        return response_user_id;
    }

    public void setResponse_user_id(long response_user_id) {
        this.response_user_id = response_user_id;
    }

    public long getCase_index_id() {
        return case_index_id;
    }

    public void setCase_index_id(long case_index_id) {
        this.case_index_id = case_index_id;
    }

    public String getCreated_type() {
        return created_type;
    }

    public void setCreated_type(String created_type) {
        this.created_type = created_type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getSharetimestamp() {
        return sharetimestamp;
    }

    public void setSharetimestamp(long sharetimestamp) {
        this.sharetimestamp = sharetimestamp;
    }

    public long getSharefinish() {
        return sharefinish;
    }

    public void setSharefinish(long sharefinish) {
        this.sharefinish = sharefinish;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public long getUpdate_timestamp() {
        return update_timestamp;
    }

    public void setUpdate_timestamp(long update_timestamp) {
        this.update_timestamp = update_timestamp;
    }

    public long getUpdate_by() {
        return update_by;
    }

    public void setUpdate_by(long update_by) {
        this.update_by = update_by;
    }

    public String getDeclinenote() {
        return declinenote;
    }

    public void setDeclinenote(String declinenote) {
        this.declinenote = declinenote;
    }

    public long getCompany_unit_id() {
        return company_unit_id;
    }

    public void setCompany_unit_id(long company_unit_id) {
        this.company_unit_id = company_unit_id;
    }

    public long getFrom_request_id() {
        return from_request_id;
    }

    public void setFrom_request_id(long from_request_id) {
        this.from_request_id = from_request_id;
    }
}