package kz.aitu.real_archive_project.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class Users {
    @Id
    private long id;
    private long auth_id;
    private String name;
    private String fullname;
    private String surname;
    private String secondname;
    private String status;
    private long company_unit_id;
    private String password;
    private long last_login_timestamp;
    private String iin;
    private boolean is_active;
    private boolean is_activated;
    private long created_timestamp;
    private long created_by;
    private long updated_timestamp;
    private long updated_by;

    public Users(long id, long auth_id, String name, String fullname, String surname, String secondname,
                 String status, long company_unit_id, String password, long last_login_timestamp, String iin,
                 boolean is_active, boolean is_activated, long created_timestamp, long created_by,
                 long updated_timestamp, long updated_by) {
        this.id = id;
        this.auth_id = auth_id;
        this.name = name;
        this.fullname = fullname;
        this.surname = surname;
        this.secondname = secondname;
        this.status = status;
        this.company_unit_id = company_unit_id;
        this.password = password;
        this.last_login_timestamp = last_login_timestamp;
        this.iin = iin;
        this.is_active = is_active;
        this.is_activated = is_activated;
        this.created_timestamp = created_timestamp;
        this.created_by = created_by;
        this.updated_timestamp = updated_timestamp;
        this.updated_by = updated_by;
    }

    public Users() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAuth_id() {
        return auth_id;
    }

    public void setAuth_id(long auth_id) {
        this.auth_id = auth_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getCompany_unit_id() {
        return company_unit_id;
    }

    public void setCompany_unit_id(long company_unit_id) {
        this.company_unit_id = company_unit_id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getLast_login_timestamp() {
        return last_login_timestamp;
    }

    public void setLast_login_timestamp(long last_login_timestamp) {
        this.last_login_timestamp = last_login_timestamp;
    }

    public String getIin() {
        return iin;
    }

    public void setIin(String iin) {
        this.iin = iin;
    }

    public boolean isIs_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    public boolean isIs_activated() {
        return is_activated;
    }

    public void setIs_activated(boolean is_activated) {
        this.is_activated = is_activated;
    }

    public long getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(long created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public long getCreated_by() {
        return created_by;
    }

    public void setCreated_by(long created_by) {
        this.created_by = created_by;
    }

    public long getUpdated_timestamp() {
        return updated_timestamp;
    }

    public void setUpdated_timestamp(long updated_timestamp) {
        this.updated_timestamp = updated_timestamp;
    }

    public long getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(long updated_by) {
        this.updated_by = updated_by;
    }
}