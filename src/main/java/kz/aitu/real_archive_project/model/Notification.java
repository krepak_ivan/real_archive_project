package kz.aitu.real_archive_project.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "notification")
public class Notification {
    @Id
    private long id;
    private String object_type;
    private long object_id;
    private long company_unit_id;
    private long user_id;
    private long created_timestamp;
    private long veiwed_timestamp;
    private boolean is_viewed;
    private String title;
    private String text_of_notification;
    private long company_id;

    public Notification(long id, String object_type, long object_id, long company_unit_id, long user_id,
                        long created_timestamp, long veiwed_timestamp, boolean is_viewed, String title,
                        String text_of_notification, long company_id) {
        this.id = id;
        this.object_type = object_type;
        this.object_id = object_id;
        this.company_unit_id = company_unit_id;
        this.user_id = user_id;
        this.created_timestamp = created_timestamp;
        this.veiwed_timestamp = veiwed_timestamp;
        this.is_viewed = is_viewed;
        this.title = title;
        this.text_of_notification = text_of_notification;
        this.company_id = company_id;
    }

    public Notification() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getObject_type() {
        return object_type;
    }

    public void setObject_type(String object_type) {
        this.object_type = object_type;
    }

    public long getObject_id() {
        return object_id;
    }

    public void setObject_id(long object_id) {
        this.object_id = object_id;
    }

    public long getCompany_unit_id() {
        return company_unit_id;
    }

    public void setCompany_unit_id(long company_unit_id) {
        this.company_unit_id = company_unit_id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public long getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(long created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public long getVeiwed_timestamp() {
        return veiwed_timestamp;
    }

    public void setVeiwed_timestamp(long veiwed_timestamp) {
        this.veiwed_timestamp = veiwed_timestamp;
    }

    public boolean isIs_viewed() {
        return is_viewed;
    }

    public void setIs_viewed(boolean is_viewed) {
        this.is_viewed = is_viewed;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText_of_notification() {
        return text_of_notification;
    }

    public void setText_of_notification(String text_of_notification) {
        this.text_of_notification = text_of_notification;
    }

    public long getCompany_id() {
        return company_id;
    }

    public void setCompany_id(long company_id) {
        this.company_id = company_id;
    }
}