package kz.aitu.real_archive_project.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "catalog_case")
public class CatalogCase {
    @Id
    private long id;
    private long case_id;
    private long catalog_id;
    private long company_unit_id;
    private long created_timestamp;
    private long created_by;
    private long updated_timestamp;
    private long updated_by;

    public CatalogCase(long id, long case_id, long catalog_id, long company_unit_id, long created_timestamp,
                       long created_by, long updated_timestamp, long updated_by) {
        this.id = id;
        this.case_id = case_id;
        this.catalog_id = catalog_id;
        this.company_unit_id = company_unit_id;
        this.created_timestamp = created_timestamp;
        this.created_by = created_by;
        this.updated_timestamp = updated_timestamp;
        this.updated_by = updated_by;
    }

    public CatalogCase() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCase_id() {
        return case_id;
    }

    public void setCase_id(long case_id) {
        this.case_id = case_id;
    }

    public long getCatalog_id() {
        return catalog_id;
    }

    public void setCatalog_id(long catalog_id) {
        this.catalog_id = catalog_id;
    }

    public long getCompany_unit_id() {
        return company_unit_id;
    }

    public void setCompany_unit_id(long company_unit_id) {
        this.company_unit_id = company_unit_id;
    }

    public long getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(long created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public long getCreated_by() {
        return created_by;
    }

    public void setCreated_by(long created_by) {
        this.created_by = created_by;
    }

    public long getUpdated_timestamp() {
        return updated_timestamp;
    }

    public void setUpdated_timestamp(long updated_timestamp) {
        this.updated_timestamp = updated_timestamp;
    }

    public long getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(long updated_by) {
        this.updated_by = updated_by;
    }
}