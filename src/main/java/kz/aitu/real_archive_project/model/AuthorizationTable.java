package kz.aitu.real_archive_project.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "authorization_table")
public class AuthorizationTable {
    @Id
    private long id;
    private String username;
    private String email;
    private String password;
    private String role;
    private String forgot_password_key;
    private long forgot_password_timestamp;
    private long company_unit_id;

    public AuthorizationTable(long id, String username, String email, String password, String role,
                              String forgot_password_key, long forgot_password_timestamp, long company_unit_id) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.role = role;
        this.forgot_password_key = forgot_password_key;
        this.forgot_password_timestamp = forgot_password_timestamp;
        this.company_unit_id = company_unit_id;
    }

    public AuthorizationTable() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getForgot_password_key() {
        return forgot_password_key;
    }

    public void setForgot_password_key(String forgot_password_key) {
        this.forgot_password_key = forgot_password_key;
    }

    public long getForgot_password_timestamp() {
        return forgot_password_timestamp;
    }

    public void setForgot_password_timestamp(long forgot_password_timestamp) {
        this.forgot_password_timestamp = forgot_password_timestamp;
    }

    public long getCompany_unit_id() {
        return company_unit_id;
    }

    public void setCompany_unit_id(long company_unit_id) {
        this.company_unit_id = company_unit_id;
    }
}