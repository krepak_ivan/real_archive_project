package kz.aitu.real_archive_project.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "file_routing")
public class FileRouting {
    @Id
    private long id;
    private long file_id;
    private String table_name;
    private long table_id;
    private String type;

    public FileRouting(long id, long file_id, String table_name, long table_id, String type) {
        this.id = id;
        this.file_id = file_id;
        this.table_name = table_name;
        this.table_id = table_id;
        this.type = type;
    }

    public FileRouting() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getFile_id() {
        return file_id;
    }

    public void setFile_id(long file_id) {
        this.file_id = file_id;
    }

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public long getTable_id() {
        return table_id;
    }

    public void setTable_id(long table_id) {
        this.table_id = table_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}