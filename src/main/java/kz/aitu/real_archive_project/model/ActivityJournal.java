package kz.aitu.real_archive_project.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "activity_journal")
public class ActivityJournal {
    @Id
    private long id;
    private String type_of_event;
    private String object_type;
    private long object_id;
    private long created_timestamp;
    private long created_by;
    private String message_level;
    private String message;

    public ActivityJournal(long id, String type_of_event, String object_type, long object_id, long created_timestamp,
                           long created_by, String message_level, String message) {
        this.id = id;
        this.type_of_event = type_of_event;
        this.object_type = object_type;
        this.object_id = object_id;
        this.created_timestamp = created_timestamp;
        this.created_by = created_by;
        this.message_level = message_level;
        this.message = message;
    }

    public ActivityJournal() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType_of_event() {
        return type_of_event;
    }

    public void setType_of_event(String type_of_event) {
        this.type_of_event = type_of_event;
    }

    public String getObject_type() {
        return object_type;
    }

    public void setObject_type(String object_type) {
        this.object_type = object_type;
    }

    public long getObject_id() {
        return object_id;
    }

    public void setObject_id(long object_id) {
        this.object_id = object_id;
    }

    public long getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(long created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public long getCreated_by() {
        return created_by;
    }

    public void setCreated_by(long created_by) {
        this.created_by = created_by;
    }

    public String getMessage_level() {
        return message_level;
    }

    public void setMessage_level(String message_level) {
        this.message_level = message_level;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}