package kz.aitu.real_archive_project.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "nomenclature_summary")
public class NomenclatureSummary {
    @Id
    private long id;
    private String number_of_nomenclature_summary;
    private int year;
    private long company_unit_id;
    private long created_timestamp;
    private long created_by;
    private long updated_timestamp;
    private long updated_by;

    public NomenclatureSummary(long id, String number_of_nomenclature_summary, int year, long company_unit_id,
                               long created_timestamp, long created_by, long updated_timestamp, long updated_by) {
        this.id = id;
        this.number_of_nomenclature_summary = number_of_nomenclature_summary;
        this.year = year;
        this.company_unit_id = company_unit_id;
        this.created_timestamp = created_timestamp;
        this.created_by = created_by;
        this.updated_timestamp = updated_timestamp;
        this.updated_by = updated_by;
    }

    public NomenclatureSummary() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumber_of_nomenclature_summary() {
        return number_of_nomenclature_summary;
    }

    public void setNumber_of_nomenclature_summary(String number_of_nomenclature_summary) {
        this.number_of_nomenclature_summary = number_of_nomenclature_summary;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public long getCompany_unit_id() {
        return company_unit_id;
    }

    public void setCompany_unit_id(long company_unit_id) {
        this.company_unit_id = company_unit_id;
    }

    public long getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(long created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public long getCreated_by() {
        return created_by;
    }

    public void setCreated_by(long created_by) {
        this.created_by = created_by;
    }

    public long getUpdated_timestamp() {
        return updated_timestamp;
    }

    public void setUpdated_timestamp(long updated_timestamp) {
        this.updated_timestamp = updated_timestamp;
    }

    public long getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(long updated_by) {
        this.updated_by = updated_by;
    }
}