package kz.aitu.real_archive_project.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "share_table")
public class ShareTable {
    @Id
    private long id;
    private long request_id;
    private String note;
    private long sender_id;
    private long receiver_id;
    private long share_timestamp;

    public ShareTable(long id, long request_id, String note, long sender_id, long receiver_id, long share_timestamp) {
        this.id = id;
        this.request_id = request_id;
        this.note = note;
        this.sender_id = sender_id;
        this.receiver_id = receiver_id;
        this.share_timestamp = share_timestamp;
    }

    public ShareTable() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRequest_id() {
        return request_id;
    }

    public void setRequest_id(long request_id) {
        this.request_id = request_id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public long getSender_id() {
        return sender_id;
    }

    public void setSender_id(long sender_id) {
        this.sender_id = sender_id;
    }

    public long getReceiver_id() {
        return receiver_id;
    }

    public void setReceiver_id(long receiver_id) {
        this.receiver_id = receiver_id;
    }

    public long getShare_timestamp() {
        return share_timestamp;
    }

    public void setShare_timestamp(long share_timestamp) {
        this.share_timestamp = share_timestamp;
    }
}