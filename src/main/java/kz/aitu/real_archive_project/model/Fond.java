package kz.aitu.real_archive_project.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "fond")
public class Fond {
    @Id
    private long id;
    private String fond_number;
    private long created_timestamp;
    private long create_by;
    private long updated_timestamp;
    private long updated_by;

    public Fond(long id, String fond_number, long created_timestamp, long create_by, long updated_timestamp,
                long updated_by) {
        this.id = id;
        this.fond_number = fond_number;
        this.created_timestamp = created_timestamp;
        this.create_by = create_by;
        this.updated_timestamp = updated_timestamp;
        this.updated_by = updated_by;
    }

    public Fond() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFond_number() {
        return fond_number;
    }

    public void setFond_number(String fond_number) {
        this.fond_number = fond_number;
    }

    public long getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(long created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public long getCreate_by() {
        return create_by;
    }

    public void setCreate_by(long create_by) {
        this.create_by = create_by;
    }

    public long getUpdated_timestamp() {
        return updated_timestamp;
    }

    public void setUpdated_timestamp(long updated_timestamp) {
        this.updated_timestamp = updated_timestamp;
    }

    public long getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(long updated_by) {
        this.updated_by = updated_by;
    }
}