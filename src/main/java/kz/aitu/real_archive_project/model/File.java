package kz.aitu.real_archive_project.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "file")
public class File {
    @Id
    private long id;
    private String name;
    private String type;
    private long size;
    private int page_count;
    private String hash;
    private boolean is_deleted;
    private long file_binary_id;
    private long created_timestamp;
    private long created_by;
    private long updated_timestamp;
    private long updated_by;

    public File(long id, String name, String type, long size, int page_count, String hash, boolean is_deleted,
                long file_binary_id, long created_timestamp, long created_by, long updated_timestamp,
                long updated_by) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.size = size;
        this.page_count = page_count;
        this.hash = hash;
        this.is_deleted = is_deleted;
        this.file_binary_id = file_binary_id;
        this.created_timestamp = created_timestamp;
        this.created_by = created_by;
        this.updated_timestamp = updated_timestamp;
        this.updated_by = updated_by;
    }

    public File() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public int getPage_count() {
        return page_count;
    }

    public void setPage_count(int page_count) {
        this.page_count = page_count;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public boolean isIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(boolean is_deleted) {
        this.is_deleted = is_deleted;
    }

    public long getFile_binary_id() {
        return file_binary_id;
    }

    public void setFile_binary_id(long file_binary_id) {
        this.file_binary_id = file_binary_id;
    }

    public long getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(long created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public long getCreated_by() {
        return created_by;
    }

    public void setCreated_by(long created_by) {
        this.created_by = created_by;
    }

    public long getUpdated_timestamp() {
        return updated_timestamp;
    }

    public void setUpdated_timestamp(long updated_timestamp) {
        this.updated_timestamp = updated_timestamp;
    }

    public long getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(long updated_by) {
        this.updated_by = updated_by;
    }
}