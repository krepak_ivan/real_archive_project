package kz.aitu.real_archive_project.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "case_index")
public class CaseIndex {
    @Id
    private long id;
    private String case_index;
    private String title_ru;
    private String title_kz;
    private String title_en;
    private int storage_type;
    private int storage_year;
    private String note;
    private long company_unit_id;
    private long nomenclature_id;
    private long created_timestamp;
    private long created_by;
    private long updated_timestamp;
    private long updated_by;

    public CaseIndex(long id, String case_index, String title_ru, String title_kz, String title_en, int storage_type,
                     int storage_year, String note, long company_unit_id, long nomenclature_id,
                     long created_timestamp, long created_by, long updated_timestamp, long updated_by) {
        this.id = id;
        this.case_index = case_index;
        this.title_ru = title_ru;
        this.title_kz = title_kz;
        this.title_en = title_en;
        this.storage_type = storage_type;
        this.storage_year = storage_year;
        this.note = note;
        this.company_unit_id = company_unit_id;
        this.nomenclature_id = nomenclature_id;
        this.created_timestamp = created_timestamp;
        this.created_by = created_by;
        this.updated_timestamp = updated_timestamp;
        this.updated_by = updated_by;
    }

    public CaseIndex() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCase_index() {
        return case_index;
    }

    public void setCase_index(String case_index) {
        this.case_index = case_index;
    }

    public String getTitle_ru() {
        return title_ru;
    }

    public void setTitle_ru(String title_ru) {
        this.title_ru = title_ru;
    }

    public String getTitle_kz() {
        return title_kz;
    }

    public void setTitle_kz(String title_kz) {
        this.title_kz = title_kz;
    }

    public String getTitle_en() {
        return title_en;
    }

    public void setTitle_en(String title_en) {
        this.title_en = title_en;
    }

    public int getStorage_type() {
        return storage_type;
    }

    public void setStorage_type(int storage_type) {
        this.storage_type = storage_type;
    }

    public int getStorage_year() {
        return storage_year;
    }

    public void setStorage_year(int storage_year) {
        this.storage_year = storage_year;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public long getCompany_unit_id() {
        return company_unit_id;
    }

    public void setCompany_unit_id(long company_unit_id) {
        this.company_unit_id = company_unit_id;
    }

    public long getNomenclature_id() {
        return nomenclature_id;
    }

    public void setNomenclature_id(long nomenclature_id) {
        this.nomenclature_id = nomenclature_id;
    }

    public long getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(long created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public long getCreated_by() {
        return created_by;
    }

    public void setCreated_by(long created_by) {
        this.created_by = created_by;
    }

    public long getUpdated_timestamp() {
        return updated_timestamp;
    }

    public void setUpdated_timestamp(long updated_timestamp) {
        this.updated_timestamp = updated_timestamp;
    }

    public long getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(long updated_by) {
        this.updated_by = updated_by;
    }
}