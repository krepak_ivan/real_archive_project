package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.RequestTable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestTableRepository extends CrudRepository<RequestTable, Long> {
    RequestTable findById(long id);
}
