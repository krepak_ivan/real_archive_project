package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.Company;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends CrudRepository<Company, Long> {
    Company findById(long id);
}
