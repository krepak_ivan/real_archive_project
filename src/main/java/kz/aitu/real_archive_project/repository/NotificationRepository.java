package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.Notification;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationRepository extends CrudRepository<Notification, Long> {
    Notification findById(long id);
}
