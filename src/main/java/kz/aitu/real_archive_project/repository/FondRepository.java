package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.Fond;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FondRepository extends CrudRepository<Fond, Long> {
    Fond findById(long id);
}
