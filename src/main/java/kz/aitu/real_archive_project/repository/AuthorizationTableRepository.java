package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.AuthorizationTable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorizationTableRepository extends CrudRepository<AuthorizationTable, Long> {
    AuthorizationTable findById(long id);
}
