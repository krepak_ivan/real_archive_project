package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.ActivityJournal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityJournalRepository extends CrudRepository<ActivityJournal, Long> {
    ActivityJournal findById(long id);
}

//comment to test
