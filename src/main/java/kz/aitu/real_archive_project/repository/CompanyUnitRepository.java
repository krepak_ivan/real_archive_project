package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.CompanyUnit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyUnitRepository extends CrudRepository<CompanyUnit, Long> {
    CompanyUnit findById(long id);
}
