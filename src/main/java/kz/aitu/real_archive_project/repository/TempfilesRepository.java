package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.Tempfiles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TempfilesRepository extends CrudRepository<Tempfiles, Long> {
    Tempfiles findById(long id);
}
