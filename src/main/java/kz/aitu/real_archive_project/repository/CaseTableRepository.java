package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.CaseTable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaseTableRepository extends CrudRepository<CaseTable, Long> {
    CaseTable findById(long id);
}
