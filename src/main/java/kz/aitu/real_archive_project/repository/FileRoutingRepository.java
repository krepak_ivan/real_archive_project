package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.FileRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRoutingRepository extends CrudRepository<FileRouting, Long> {
    FileRouting findById(long id);
}
