package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.Location;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends CrudRepository<Location, Long> {
    Location findById(long id);
}
