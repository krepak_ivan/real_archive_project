package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.CatalogCase;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CatalogCaseRepository extends CrudRepository<CatalogCase, Long> {
    CatalogCase findById(long id);
}