package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.Catalog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CatalogRepository extends CrudRepository<Catalog, Long> {
    Catalog findById(long id);
}
