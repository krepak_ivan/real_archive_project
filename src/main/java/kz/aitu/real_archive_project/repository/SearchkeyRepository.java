package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.Searchkey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchkeyRepository extends CrudRepository<Searchkey, Long> {
    Searchkey findById(long id);
}
