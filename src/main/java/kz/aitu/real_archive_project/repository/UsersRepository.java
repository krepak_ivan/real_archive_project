package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.Users;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends CrudRepository<Users, Long> {
    Users findById(long id);
}
