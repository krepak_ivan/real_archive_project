package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.Record;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecordRepository extends CrudRepository<Record, Long> {
    Record findById(long id);
}
