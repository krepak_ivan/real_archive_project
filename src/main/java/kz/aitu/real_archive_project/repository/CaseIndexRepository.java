package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.CaseIndex;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaseIndexRepository extends CrudRepository<CaseIndex, Long> {
    CaseIndex findById(long id);
}