package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.NomenclatureSummary;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NomenclatureSummaryRepository extends CrudRepository<NomenclatureSummary, Long> {
    NomenclatureSummary findById(long id);
}
