package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.ShareTable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShareTableRepository extends CrudRepository<ShareTable, Long> {
    ShareTable findById(long id);
}
