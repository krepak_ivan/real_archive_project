package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.SearchKeyRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchKeyRoutingRepository extends CrudRepository<SearchKeyRouting, Long> {
    SearchKeyRouting findById(long id);
}
