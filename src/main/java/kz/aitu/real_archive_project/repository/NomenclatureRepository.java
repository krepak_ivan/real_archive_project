package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.Nomenclature;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NomenclatureRepository extends CrudRepository<Nomenclature, Long> {
    Nomenclature findById(long id);
}
