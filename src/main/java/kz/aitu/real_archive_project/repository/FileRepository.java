package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.File;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepository extends CrudRepository<File, Long> {
    File findById(long id);
}
