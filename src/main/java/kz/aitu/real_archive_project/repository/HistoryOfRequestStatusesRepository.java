package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.HistoryOfRequestStatuses;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HistoryOfRequestStatusesRepository extends CrudRepository<HistoryOfRequestStatuses, Long> {
    HistoryOfRequestStatuses findById(long id);
}
