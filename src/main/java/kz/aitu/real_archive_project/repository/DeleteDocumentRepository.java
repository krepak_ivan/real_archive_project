package kz.aitu.real_archive_project.repository;

import kz.aitu.real_archive_project.model.DeleteDocument;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeleteDocumentRepository extends CrudRepository<DeleteDocument, Long> {
    DeleteDocument findById(long id);
}
