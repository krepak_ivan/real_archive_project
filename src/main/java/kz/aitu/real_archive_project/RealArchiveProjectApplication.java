package kz.aitu.real_archive_project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RealArchiveProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(RealArchiveProjectApplication.class, args);
	}

}