package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.Tempfiles;
import kz.aitu.real_archive_project.repository.TempfilesRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TempfilesService {
    private final TempfilesRepository tempfilesRepository;

    public TempfilesService(TempfilesRepository tempfilesRepository) {
        this.tempfilesRepository = tempfilesRepository;
    }

    public List<Tempfiles> getAll(){
        return (List<Tempfiles>) tempfilesRepository.findAll();
    }

    public Tempfiles getById(long id){
        return tempfilesRepository.findById(id);
    }

    public void deleteById(long id){
        tempfilesRepository.deleteById(id);
    }

    public Tempfiles createTempfiles(Tempfiles tempfiles){
        return tempfilesRepository.save(tempfiles);
    }
}