package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.CatalogCase;
import kz.aitu.real_archive_project.repository.CatalogCaseRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CatalogCaseService {
    private final CatalogCaseRepository catalogCaseRepository;

    public CatalogCaseService(CatalogCaseRepository catalogCaseRepository) {
        this.catalogCaseRepository = catalogCaseRepository;
    }

    public List<CatalogCase> getAll(){
        return (List<CatalogCase>) catalogCaseRepository.findAll();
    }

    public CatalogCase getById(long id){
        return catalogCaseRepository.findById(id);
    }

    public void deleteById(long id){
        catalogCaseRepository.deleteById(id);
    }

    public CatalogCase createCatalogCase(CatalogCase catalogCase){
        return catalogCaseRepository.save(catalogCase);
    }
}