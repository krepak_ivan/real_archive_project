package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.Notification;
import kz.aitu.real_archive_project.model.Record;
import kz.aitu.real_archive_project.repository.RecordRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecordService {
    private final RecordRepository recordRepository;

    public RecordService(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }

    public List<Record> getAll(){
        return (List<Record>) recordRepository.findAll();
    }

    public Record getById(long id){
        return recordRepository.findById(id);
    }

    public void deleteById(long id){
        recordRepository.deleteById(id);
    }

    public Record createRecord(Record record){
        return recordRepository.save(record);
    }
}