package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.Location;
import kz.aitu.real_archive_project.repository.LocationRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationService {
    private final LocationRepository locationRepository;

    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public List<Location> getAll(){
        return (List<Location>) locationRepository.findAll();
    }

    public Location getById(long id){
        return locationRepository.findById(id);
    }

    public void deleteById(long id){
        locationRepository.deleteById(id);
    }

    public Location createLocation(Location location){
        return locationRepository.save(location);
    }
}