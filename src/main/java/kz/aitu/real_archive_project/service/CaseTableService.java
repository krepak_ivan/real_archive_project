package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.CaseTable;
import kz.aitu.real_archive_project.repository.CaseTableRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CaseTableService {
    private final CaseTableRepository caseTableRepository;

    public CaseTableService(CaseTableRepository caseTableRepository) {
        this.caseTableRepository = caseTableRepository;
    }

    public List<CaseTable> getAll(){
        return (List<CaseTable>) caseTableRepository.findAll();
    }

    public CaseTable getById(long id){
        return caseTableRepository.findById(id);
    }

    public void deleteById(long id){
        caseTableRepository.deleteById(id);
    }

    public CaseTable createCaseTable(CaseTable caseTable){
        return caseTableRepository.save(caseTable);
    }
}