package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.File;
import kz.aitu.real_archive_project.repository.FileRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileService {
    private final FileRepository fileRepository;

    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public List<File> getAll(){
        return (List<File>) fileRepository.findAll();
    }

    public File getById(long id){
        return fileRepository.findById(id);
    }

    public void deleteById(long id){
        fileRepository.deleteById(id);
    }

    public File createFile(File file){
        return fileRepository.save(file);
    }
}