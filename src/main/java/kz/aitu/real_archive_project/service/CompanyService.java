package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.Company;
import kz.aitu.real_archive_project.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {
    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<Company> getAll(){
        return (List<Company>) companyRepository.findAll();
    }

    public Company getById(long id){
        return companyRepository.findById(id);
    }

    public void deleteById(long id){
        companyRepository.deleteById(id);
    }

    public Company createCompany(Company company){
        return companyRepository.save(company);
    }
}