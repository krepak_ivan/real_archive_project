package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.Catalog;
import kz.aitu.real_archive_project.repository.CatalogRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CatalogService {
    private final CatalogRepository catalogRepository;

    public CatalogService(CatalogRepository catalogRepository) {
        this.catalogRepository = catalogRepository;
    }

    public List<Catalog> getAll(){
        return (List<Catalog>) catalogRepository.findAll();
    }

    public Catalog getById(long id){
        return catalogRepository.findById(id);
    }

    public void deleteById(long id){
        catalogRepository.deleteById(id);
    }

    public Catalog createCatalog(Catalog catalog){
        return catalogRepository.save(catalog);
    }
}