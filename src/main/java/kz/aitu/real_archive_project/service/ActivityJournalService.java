package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.AuthorizationTable;
import kz.aitu.real_archive_project.repository.ActivityJournalRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityJournalService {
    private final ActivityJournalRepository activityJournalRepository;

    public ActivityJournalService(ActivityJournalRepository activityJournalRepository) {
        this.activityJournalRepository = activityJournalRepository;
    }

    public List<ActivityJournal> getAll(){
        return (List<ActivityJournal>) activityJournalRepository.findAll();
    }

    public ActivityJournal getById(long id){
        return activityJournalRepository.findById(id);
    }

    public void deleteById(long id){
        activityJournalRepository.deleteById(id);
    }

    public ActivityJournal createActivityJournal(ActivityJournal activityJournal){
        return activityJournalRepository.save(activityJournal);
    }
}
