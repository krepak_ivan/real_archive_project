package kz.aitu.real_archive_project.service;


import kz.aitu.real_archive_project.model.RequestTable;
import kz.aitu.real_archive_project.repository.RequestTableRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RequestTableService {
    private final RequestTableRepository requestTableRepository;

    public RequestTableService(RequestTableRepository requestTableRepository) {
        this.requestTableRepository = requestTableRepository;
    }

    public List<RequestTable> getAll(){
        return (List<RequestTable>) requestTableRepository.findAll();
    }

    public RequestTable getById(long id){
        return requestTableRepository.findById(id);
    }

    public void deleteById(long id){
        requestTableRepository.deleteById(id);
    }

    public RequestTable createRequestTable(RequestTable requestTable){
        return requestTableRepository.save(requestTable);
    }
}
