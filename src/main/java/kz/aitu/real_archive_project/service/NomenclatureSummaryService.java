package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.AuthorizationTable;
import kz.aitu.real_archive_project.model.NomenclatureSummary;
import kz.aitu.real_archive_project.repository.NomenclatureSummaryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NomenclatureSummaryService {
    private final NomenclatureSummaryRepository nomenclatureSummaryRepository;

    public NomenclatureSummaryService(NomenclatureSummaryRepository nomenclatureSummaryRepository) {
        this.nomenclatureSummaryRepository = nomenclatureSummaryRepository;
    }

    public List<NomenclatureSummary> getAll(){
        return (List<NomenclatureSummary>) nomenclatureSummaryRepository.findAll();
    }

    public NomenclatureSummary getById(long id){
        return nomenclatureSummaryRepository.findById(id);
    }

    public void deleteById(long id){
        nomenclatureSummaryRepository.deleteById(id);
    }

    public NomenclatureSummary createNomenclatureSummary(NomenclatureSummary nomenclatureSummary){
        return nomenclatureSummaryRepository.save(nomenclatureSummary);
    }
}