package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.AuthorizationTable;
import kz.aitu.real_archive_project.model.Fond;
import kz.aitu.real_archive_project.repository.FondRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FondService {
    private final FondRepository fondRepository;

    public FondService(FondRepository fondRepository) {
        this.fondRepository = fondRepository;
    }

    public List<Fond> getAll(){
        return (List<Fond>) fondRepository.findAll();
    }

    public Fond getById(long id){
        return fondRepository.findById(id);
    }

    public void deleteById(long id){
        fondRepository.deleteById(id);
    }

    public Fond createFond(Fond fond){
        return fondRepository.save(fond);
    }
}