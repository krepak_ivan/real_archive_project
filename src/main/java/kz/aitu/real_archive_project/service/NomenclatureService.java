package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.Location;
import kz.aitu.real_archive_project.model.Nomenclature;
import kz.aitu.real_archive_project.repository.NomenclatureRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NomenclatureService {
    private final NomenclatureRepository nomenclatureRepository;

    public NomenclatureService(NomenclatureRepository nomenclatureRepository) {
        this.nomenclatureRepository = nomenclatureRepository;
    }

    public List<Nomenclature> getAll(){
        return (List<Nomenclature>) nomenclatureRepository.findAll();
    }

    public Nomenclature getById(long id){
        return nomenclatureRepository.findById(id);
    }

    public void deleteById(long id){
        nomenclatureRepository.deleteById(id);
    }

    public Nomenclature createNomenclature(Nomenclature nomenclature){
        return nomenclatureRepository.save(nomenclature);
    }
}