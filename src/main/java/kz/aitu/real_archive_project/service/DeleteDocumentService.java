package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.DeleteDocument;
import kz.aitu.real_archive_project.repository.DeleteDocumentRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeleteDocumentService {
    private final DeleteDocumentRepository deleteDocumentRepository;

    public DeleteDocumentService(DeleteDocumentRepository deleteDocumentRepository) {
        this.deleteDocumentRepository = deleteDocumentRepository;
    }

    public List<DeleteDocument> getAll(){
        return (List<DeleteDocument>) deleteDocumentRepository.findAll();
    }

    public DeleteDocument getById(long id){
        return deleteDocumentRepository.findById(id);
    }

    public void deleteById(long id){
        deleteDocumentRepository.deleteById(id);
    }

    public DeleteDocument createDeleteDocument(DeleteDocument deleteDocument){
        return deleteDocumentRepository.save(deleteDocument);
    }
}