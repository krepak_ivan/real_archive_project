package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.AuthorizationTable;
import kz.aitu.real_archive_project.model.CaseIndex;
import kz.aitu.real_archive_project.repository.CaseIndexRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CaseIndexService {
    final private CaseIndexRepository caseIndexRepository;

    public CaseIndexService(CaseIndexRepository caseIndexRepository) {
        this.caseIndexRepository = caseIndexRepository;
    }

    public List<CaseIndex> getAll(){
        return (List<CaseIndex>) caseIndexRepository.findAll();
    }

    public CaseIndex getById(long id){
        return caseIndexRepository.findById(id);
    }

    public void deleteById(long id){
        caseIndexRepository.deleteById(id);
    }

    public CaseIndex createCaseIndex(CaseIndex caseIndex){
        return caseIndexRepository.save(caseIndex);
    }
}