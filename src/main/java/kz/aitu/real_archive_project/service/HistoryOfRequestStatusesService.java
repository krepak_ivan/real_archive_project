package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.AuthorizationTable;
import kz.aitu.real_archive_project.model.HistoryOfRequestStatuses;
import kz.aitu.real_archive_project.repository.HistoryOfRequestStatusesRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HistoryOfRequestStatusesService {
    private final HistoryOfRequestStatusesRepository historyOfRequestStatusesRepository;

    public HistoryOfRequestStatusesService(HistoryOfRequestStatusesRepository historyOfRequestStatusesRepository) {
        this.historyOfRequestStatusesRepository = historyOfRequestStatusesRepository;
    }

    public List<HistoryOfRequestStatuses> getAll(){
        return (List<HistoryOfRequestStatuses>) historyOfRequestStatusesRepository.findAll();
    }

    public HistoryOfRequestStatuses getById(long id){
        return historyOfRequestStatusesRepository.findById(id);
    }

    public void deleteById(long id){
        historyOfRequestStatusesRepository.deleteById(id);
    }

    public HistoryOfRequestStatuses createHistoryOfRequestStatuses(HistoryOfRequestStatuses historyOfRequestStatuses){
        return historyOfRequestStatusesRepository.save(historyOfRequestStatuses);
    }
}
