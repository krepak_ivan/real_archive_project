package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.RequestTable;
import kz.aitu.real_archive_project.model.SearchKeyRouting;
import kz.aitu.real_archive_project.repository.SearchKeyRoutingRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchKeyRoutingService {
    private final SearchKeyRoutingRepository searchKeyRoutingRepository;

    public SearchKeyRoutingService(SearchKeyRoutingRepository searchKeyRoutingRepository) {
        this.searchKeyRoutingRepository = searchKeyRoutingRepository;
    }

    public List<SearchKeyRouting> getAll(){
        return (List<SearchKeyRouting>) searchKeyRoutingRepository.findAll();
    }

    public SearchKeyRouting getById(long id){
        return searchKeyRoutingRepository.findById(id);
    }

    public void deleteById(long id){
        searchKeyRoutingRepository.deleteById(id);
    }

    public SearchKeyRouting createSearchKeyRouting(SearchKeyRouting searchKeyRouting){
        return searchKeyRoutingRepository.save(searchKeyRouting);
    }
}