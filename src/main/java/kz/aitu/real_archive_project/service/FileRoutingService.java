package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.FileRouting;
import kz.aitu.real_archive_project.repository.FileRoutingRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileRoutingService {
    private final FileRoutingRepository fileRoutingRepository;

    public FileRoutingService(FileRoutingRepository fileRoutingRepository) {
        this.fileRoutingRepository = fileRoutingRepository;
    }

    public List<FileRouting> getAll(){
        return (List<FileRouting>) fileRoutingRepository.findAll();
    }

    public FileRouting getById(long id){
        return fileRoutingRepository.findById(id);
    }

    public void deleteById(long id){
        fileRoutingRepository.deleteById(id);
    }

    public FileRouting createFileRouting(FileRouting fileRouting){
        return fileRoutingRepository.save(fileRouting);
    }
}