package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.Users;
import kz.aitu.real_archive_project.repository.UsersRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersService {
    private final UsersRepository usersRepository;

    public UsersService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public List<Users> getAll(){
        return (List<Users>) usersRepository.findAll();
    }

    public Users getById(long id){
        return usersRepository.findById(id);
    }

    public void deleteById(long id){
        usersRepository.deleteById(id);
    }

    public Users createUsers(Users users){
        return usersRepository.save(users);
    }
}