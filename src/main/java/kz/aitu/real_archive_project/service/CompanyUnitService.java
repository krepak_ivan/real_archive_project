package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.CompanyUnit;
import kz.aitu.real_archive_project.repository.CompanyUnitRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyUnitService {
    private final CompanyUnitRepository companyUnitRepository;

    public CompanyUnitService(CompanyUnitRepository companyUnitRepository) {
        this.companyUnitRepository = companyUnitRepository;
    }

    public List<CompanyUnit> getAll(){
        return (List<CompanyUnit>) companyUnitRepository.findAll();
    }

    public CompanyUnit getById(long id){
        return companyUnitRepository.findById(id);
    }

    public void deleteById(long id){
        companyUnitRepository.deleteById(id);
    }

    public CompanyUnit createCompanyUnit(CompanyUnit companyUnit){
        return companyUnitRepository.save(companyUnit);
    }
}
