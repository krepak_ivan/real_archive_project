package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.AuthorizationTable;
import kz.aitu.real_archive_project.repository.AuthorizationTableRepository;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class AuthorizationTableService {
    private final AuthorizationTableRepository authorizationTableRepository;

    public AuthorizationTableService(AuthorizationTableRepository authorizationTableRepository) {
        this.authorizationTableRepository = authorizationTableRepository;
    }

    public List<AuthorizationTable> getAll(){
        return (List<AuthorizationTable>) authorizationTableRepository.findAll();
    }

    public AuthorizationTable getById(long id){
        return authorizationTableRepository.findById(id);
    }

    public void deleteById(long id){
        authorizationTableRepository.deleteById(id);
    }

    public AuthorizationTable createAuthorizationTable(AuthorizationTable authorizationTable){
        return authorizationTableRepository.save(authorizationTable);
    }

}