package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.Notification;
import kz.aitu.real_archive_project.model.RequestTable;
import kz.aitu.real_archive_project.repository.NotificationRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationService {
    private final NotificationRepository notificationRepository;

    public NotificationService(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    public List<Notification> getAll(){
        return (List<Notification>) notificationRepository.findAll();
    }

    public Notification getById(long id){
        return notificationRepository.findById(id);
    }

    public void deleteById(long id){
        notificationRepository.deleteById(id);
    }

    public Notification createNotification(Notification notification){
        return notificationRepository.save(notification);
    }
}
