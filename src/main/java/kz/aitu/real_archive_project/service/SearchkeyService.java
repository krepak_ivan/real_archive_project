package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.RequestTable;
import kz.aitu.real_archive_project.model.Searchkey;
import kz.aitu.real_archive_project.repository.SearchkeyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchkeyService {
    private final SearchkeyRepository searchkeyRepository;

    public SearchkeyService(SearchkeyRepository searchkeyRepository) {
        this.searchkeyRepository = searchkeyRepository;
    }

    public List<Searchkey> getAll(){
        return (List<Searchkey>) searchkeyRepository.findAll();
    }

    public Searchkey getById(long id){
        return searchkeyRepository.findById(id);
    }

    public void deleteById(long id){
        searchkeyRepository.deleteById(id);
    }

    public Searchkey createSearchkey(Searchkey searchkey){
        return searchkeyRepository.save(searchkey);
    }
}