package kz.aitu.real_archive_project.service;

import kz.aitu.real_archive_project.model.RequestTable;
import kz.aitu.real_archive_project.model.ShareTable;
import kz.aitu.real_archive_project.repository.ShareTableRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShareTableService {
    private final ShareTableRepository shareTableRepository;

    public ShareTableService(ShareTableRepository shareTableRepository) {
        this.shareTableRepository = shareTableRepository;
    }

    public List<ShareTable> getAll(){
        return (List<ShareTable>) shareTableRepository.findAll();
    }

    public ShareTable getById(long id){
        return shareTableRepository.findById(id);
    }

    public void deleteById(long id){
        shareTableRepository.deleteById(id);
    }

    public ShareTable createShareTable(ShareTable shareTable){
        return shareTableRepository.save(shareTable);
    }
}
