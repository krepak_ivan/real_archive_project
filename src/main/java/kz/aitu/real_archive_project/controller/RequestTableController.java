package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.AuthorizationTable;
import kz.aitu.real_archive_project.model.RequestTable;
import kz.aitu.real_archive_project.service.RequestTableService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RequestTableController {
    private final RequestTableService requestTableService;

    public RequestTableController(RequestTableService requestTableService) {
        this.requestTableService = requestTableService;
    }

    @GetMapping("api/request_table/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(requestTableService.getById(id));
    }

    @GetMapping("api/request_table")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(requestTableService.getAll());
    }

    @DeleteMapping("api/request_table/{id}")
    public void deleteById(@PathVariable long id){
        requestTableService.deleteById(id);
    }

    @PostMapping("api/request_table")
    public ResponseEntity<?> createAuth(@RequestBody RequestTable requestTable){
        return ResponseEntity.ok(requestTableService.createRequestTable(requestTable));
    }
}
