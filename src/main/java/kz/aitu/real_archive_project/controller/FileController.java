package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.File;
import kz.aitu.real_archive_project.model.Tempfiles;
import kz.aitu.real_archive_project.service.FileService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FileController {
    private final FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping("api/file/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(fileService.getById(id));
    }

    @GetMapping("api/file")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(fileService.getAll());
    }

    @DeleteMapping("api/file/{id}")
    public void deleteById(@PathVariable long id){
        fileService.deleteById(id);
    }

    @PostMapping("api/file")
    public ResponseEntity<?> createAuth(@RequestBody File file){
        return ResponseEntity.ok(fileService.createFile(file));
    }
}