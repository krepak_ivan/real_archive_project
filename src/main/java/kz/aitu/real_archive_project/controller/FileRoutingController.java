package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.FileRouting;
import kz.aitu.real_archive_project.service.FileRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FileRoutingController {
    private final FileRoutingService fileRoutingService;

    public FileRoutingController(FileRoutingService fileRoutingService) {
        this.fileRoutingService = fileRoutingService;
    }

    @GetMapping("api/file_routing/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(fileRoutingService.getById(id));
    }

    @GetMapping("api/file_routing")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(fileRoutingService.getAll());
    }

    @DeleteMapping("api/file_routing/{id}")
    public void deleteById(@PathVariable long id){
        fileRoutingService.deleteById(id);
    }

    @PostMapping("api/file_routing")
    public ResponseEntity<?> createAuth(@RequestBody FileRouting fileRouting){
        return ResponseEntity.ok(fileRoutingService.createFileRouting(fileRouting));
    }
}