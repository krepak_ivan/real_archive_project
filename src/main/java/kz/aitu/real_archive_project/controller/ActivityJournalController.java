package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.AuthorizationTable;
import kz.aitu.real_archive_project.service.ActivityJournalService;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

@RestController
public class ActivityJournalController {
    private final ActivityJournalService activityJournalService;

    public ActivityJournalController(ActivityJournalService activityJournalService) {
        this.activityJournalService = activityJournalService;
    }

    @GetMapping("api/activity_journal/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(activityJournalService.getById(id));
    }

    @GetMapping("api/activity_journal")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(activityJournalService.getAll());
    }

    @DeleteMapping("api/activity_journal/{id}")
    public void deleteById(@PathVariable long id){
        activityJournalService.deleteById(id);
    }

    @PostMapping("api/activity_journal")
    public ResponseEntity<?> createAuth(@RequestBody ActivityJournal activityJournal){
        return ResponseEntity.ok(activityJournalService.createActivityJournal(activityJournal));
    }
}