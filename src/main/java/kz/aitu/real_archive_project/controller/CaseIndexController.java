package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.CaseIndex;
import kz.aitu.real_archive_project.service.CaseIndexService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CaseIndexController {
    private final CaseIndexService caseIndexService;

    public CaseIndexController(CaseIndexService caseIndexService) {
        this.caseIndexService = caseIndexService;
    }

    @GetMapping("api/case_index/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(caseIndexService.getById(id));
    }

    @GetMapping("api/case_index")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(caseIndexService.getAll());
    }

    @DeleteMapping("api/case_index/{id}")
    public void deleteById(@PathVariable long id){
        caseIndexService.deleteById(id);
    }

    @PostMapping("api/case_index")
    public ResponseEntity<?> createAuth(@RequestBody CaseIndex caseIndex){
        return ResponseEntity.ok(caseIndexService.createCaseIndex(caseIndex));
    }
}