package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.RequestTable;
import kz.aitu.real_archive_project.model.Searchkey;
import kz.aitu.real_archive_project.service.SearchkeyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchkeyController {
    private final SearchkeyService searchkeyService;

    public SearchkeyController(SearchkeyService searchkeyService) {
        this.searchkeyService = searchkeyService;
    }

    @GetMapping("api/searchkey/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(searchkeyService.getById(id));
    }

    @GetMapping("api/searchkey")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(searchkeyService.getAll());
    }

    @DeleteMapping("api/searchkey/{id}")
    public void deleteById(@PathVariable long id){
        searchkeyService.deleteById(id);
    }

    @PostMapping("api/searchkey")
    public ResponseEntity<?> createAuth(@RequestBody Searchkey searchkey){
        return ResponseEntity.ok(searchkeyService.createSearchkey(searchkey));
    }
}