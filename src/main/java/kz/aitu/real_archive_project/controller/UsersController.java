package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.Fond;
import kz.aitu.real_archive_project.model.Users;
import kz.aitu.real_archive_project.service.UsersService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UsersController {
    private final UsersService usersService;

    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping("api/users/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(usersService.getById(id));
    }

    @GetMapping("api/users")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(usersService.getAll());
    }

    @DeleteMapping("api/users/{id}")
    public void deleteById(@PathVariable long id){
        usersService.deleteById(id);
    }

    @PostMapping("api/users")
    public ResponseEntity<?> createAuth(@RequestBody Users users){
        return ResponseEntity.ok(usersService.createUsers(users));
    }
}
