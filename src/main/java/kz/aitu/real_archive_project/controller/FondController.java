package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.AuthorizationTable;
import kz.aitu.real_archive_project.model.Fond;
import kz.aitu.real_archive_project.service.FondService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FondController {
    private final FondService fondService;

    public FondController(FondService fondService) {
        this.fondService = fondService;
    }

    @GetMapping("api/fond/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(fondService.getById(id));
    }

    @GetMapping("api/fond")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(fondService.getAll());
    }

    @DeleteMapping("api/fond/{id}")
    public void deleteById(@PathVariable long id){
        fondService.deleteById(id);
    }

    @PostMapping("api/fond")
    public ResponseEntity<?> createAuth(@RequestBody Fond fond){
        return ResponseEntity.ok(fondService.createFond(fond));
    }
}
