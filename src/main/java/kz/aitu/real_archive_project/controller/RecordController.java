package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.Record;
import kz.aitu.real_archive_project.service.RecordService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RecordController {
    private final RecordService recordService;

    public RecordController(RecordService recordService) {
        this.recordService = recordService;
    }

    @GetMapping("api/record/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(recordService.getById(id));
    }

    @GetMapping("api/record")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(recordService.getAll());
    }

    @DeleteMapping("api/record/{id}")
    public void deleteById(@PathVariable long id){
        recordService.deleteById(id);
    }

    @PostMapping("api/record")
    public ResponseEntity<?> createAuth(@RequestBody Record record){
        return ResponseEntity.ok(recordService.createRecord(record));
    }
}