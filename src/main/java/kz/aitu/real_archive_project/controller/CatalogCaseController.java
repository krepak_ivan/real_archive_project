package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.CatalogCase;
import kz.aitu.real_archive_project.service.CatalogCaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CatalogCaseController {
    private final CatalogCaseService catalogCaseService;

    public CatalogCaseController(CatalogCaseService catalogCaseService) {
        this.catalogCaseService = catalogCaseService;
    }

    @GetMapping("api/catalog_case/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(catalogCaseService.getById(id));
    }

    @GetMapping("api/catalog_case")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(catalogCaseService.getAll());
    }

    @DeleteMapping("api/catalog_case/{id}")
    public void deleteById(@PathVariable long id){
        catalogCaseService.deleteById(id);
    }

    @PostMapping("api/catalog_case")
    public ResponseEntity<?> createAuth(@RequestBody CatalogCase catalogCase){
        return ResponseEntity.ok(catalogCaseService.createCatalogCase(catalogCase));
    }
}