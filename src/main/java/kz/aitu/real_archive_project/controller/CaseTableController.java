package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.CaseTable;
import kz.aitu.real_archive_project.service.CaseTableService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CaseTableController {
    private final CaseTableService caseTableService;

    public CaseTableController(CaseTableService caseTableService) {
        this.caseTableService = caseTableService;
    }

    @GetMapping("api/case_table/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(caseTableService.getById(id));
    }

    @GetMapping("api/case_table")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(caseTableService.getAll());
    }

    @DeleteMapping("api/case_table/{id}")
    public void deleteById(@PathVariable long id){
        caseTableService.deleteById(id);
    }

    @PostMapping("api/case_table")
    public ResponseEntity<?> createAuth(@RequestBody CaseTable caseTable){
        return ResponseEntity.ok(caseTableService.createCaseTable(caseTable));
    }
}