package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.Tempfiles;
import kz.aitu.real_archive_project.service.TempfilesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TempfilesController {
    private final TempfilesService tempfilesService;

    public TempfilesController(TempfilesService tempfilesService) {
        this.tempfilesService = tempfilesService;
    }

    @GetMapping("api/tempfiles/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(tempfilesService.getById(id));
    }

    @GetMapping("api/tempfiles")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(tempfilesService.getAll());
    }

    @DeleteMapping("api/tempfiles/{id}")
    public void deleteById(@PathVariable long id){
        tempfilesService.deleteById(id);
    }

    @PostMapping("api/tempfiles")
    public ResponseEntity<?> createAuth(@RequestBody Tempfiles tempfiles){
        return ResponseEntity.ok(tempfilesService.createTempfiles(tempfiles));
    }
}