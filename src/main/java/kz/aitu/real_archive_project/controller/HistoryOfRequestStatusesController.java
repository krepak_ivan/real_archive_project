package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.AuthorizationTable;
import kz.aitu.real_archive_project.model.HistoryOfRequestStatuses;
import kz.aitu.real_archive_project.service.HistoryOfRequestStatusesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class HistoryOfRequestStatusesController {
    private final HistoryOfRequestStatusesService historyOfRequestStatusesService;

    public HistoryOfRequestStatusesController(HistoryOfRequestStatusesService historyOfRequestStatusesService) {
        this.historyOfRequestStatusesService = historyOfRequestStatusesService;
    }

    @GetMapping("api/history_of_request_statuses/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(historyOfRequestStatusesService.getById(id));
    }

    @GetMapping("api/history_of_request_statuses")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(historyOfRequestStatusesService.getAll());
    }

    @DeleteMapping("api/history_of_request_statuses/{id}")
    public void deleteById(@PathVariable long id){
        historyOfRequestStatusesService.deleteById(id);
    }



    @PostMapping("api/history_of_request_statuses")
    public ResponseEntity<?> createAuth(@RequestBody HistoryOfRequestStatuses historyOfRequestStatuses){
        return ResponseEntity.ok(historyOfRequestStatusesService.
                createHistoryOfRequestStatuses(historyOfRequestStatuses));
    }
}
