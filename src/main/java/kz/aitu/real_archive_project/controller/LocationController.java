package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.Location;
import kz.aitu.real_archive_project.service.LocationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LocationController {
    private final LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping("api/location/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(locationService.getById(id));
    }

    @GetMapping("api/location")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(locationService.getAll());
    }

    @DeleteMapping("api/location/{id}")
    public void deleteById(@PathVariable long id){
        locationService.deleteById(id);
    }

    @PostMapping("api/location")
    public ResponseEntity<?> createAuth(@RequestBody Location location){
        return ResponseEntity.ok(locationService.createLocation(location));
    }
}