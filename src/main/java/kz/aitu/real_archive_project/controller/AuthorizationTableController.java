package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.AuthorizationTable;
import kz.aitu.real_archive_project.service.AuthorizationTableService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthorizationTableController {
    private final AuthorizationTableService authorizationTableService;

    public AuthorizationTableController(AuthorizationTableService authorizationTableService) {
        this.authorizationTableService = authorizationTableService;
    }

    @GetMapping("api/authorization_table/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(authorizationTableService.getById(id));
    }

    @GetMapping("api/authorization_table")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(authorizationTableService.getAll());
    }

    @DeleteMapping("api/authorization_table/{id}")
    public void deleteById(@PathVariable long id){
        authorizationTableService.deleteById(id);
    }

    @PostMapping("api/authorization_table")
    public ResponseEntity<?> createAuth(@RequestBody AuthorizationTable authorizationTable){
        return ResponseEntity.ok(authorizationTableService.createAuthorizationTable(authorizationTable));
    }

}