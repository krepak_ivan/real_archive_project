package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.Catalog;
import kz.aitu.real_archive_project.model.Nomenclature;
import kz.aitu.real_archive_project.service.NomenclatureService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NomenclatureController {
    private final NomenclatureService nomenclatureService;

    public NomenclatureController(NomenclatureService nomenclatureService) {
        this.nomenclatureService = nomenclatureService;
    }

    @GetMapping("api/nomenclature/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(nomenclatureService.getById(id));
    }

    @GetMapping("api/nomenclature")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(nomenclatureService.getAll());
    }

    @DeleteMapping("api/nomenclature/{id}")
    public void deleteById(@PathVariable long id){
        nomenclatureService.deleteById(id);
    }

    @PostMapping("api/nomenclature")
    public ResponseEntity<?> createAuth(@RequestBody Nomenclature nomenclature){
        return ResponseEntity.ok(nomenclatureService.createNomenclature(nomenclature));
    }
}