package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.Company;
import kz.aitu.real_archive_project.service.CompanyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyController {
    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping("api/company/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(companyService.getById(id));
    }

    @GetMapping("api/company")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(companyService.getAll());
    }

    @DeleteMapping("api/company/{id}")
    public void deleteById(@PathVariable long id){
        companyService.deleteById(id);
    }

    @PostMapping("api/company")
    public ResponseEntity<?> createAuth(@RequestBody Company company){
        return ResponseEntity.ok(companyService.createCompany(company));
    }
}
