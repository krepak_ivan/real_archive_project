package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.Notification;
import kz.aitu.real_archive_project.service.NotificationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NotificationController {
    private final NotificationService notificationService;

    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping("api/notification/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(notificationService.getById(id));
    }

    @GetMapping("api/notification")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(notificationService.getAll());
    }

    @DeleteMapping("api/notification/{id}")
    public void deleteById(@PathVariable long id){
        notificationService.deleteById(id);
    }

    @PostMapping("api/notification")
    public ResponseEntity<?> createAuth(@RequestBody Notification notification){
        return ResponseEntity.ok(notificationService.createNotification(notification));
    }
}
