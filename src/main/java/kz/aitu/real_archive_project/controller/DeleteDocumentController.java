package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.DeleteDocument;
import kz.aitu.real_archive_project.service.DeleteDocumentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class DeleteDocumentController {
    private final DeleteDocumentService deleteDocumentService;

    public DeleteDocumentController(DeleteDocumentService deleteDocumentService) {
        this.deleteDocumentService = deleteDocumentService;
    }

    @GetMapping("api/delete_document/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(deleteDocumentService.getById(id));
    }

    @GetMapping("api/delete_document")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(deleteDocumentService.getAll());
    }

    @DeleteMapping("api/delete_document/{id}")
    public void deleteById(@PathVariable long id){
        deleteDocumentService.deleteById(id);
    }

    @PostMapping("api/delete_document")
    public ResponseEntity<?> createAuth(@RequestBody DeleteDocument deleteDocument){
        return ResponseEntity.ok(deleteDocumentService.createDeleteDocument(deleteDocument));
    }
}