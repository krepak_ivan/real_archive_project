package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.CompanyUnit;
import kz.aitu.real_archive_project.model.NomenclatureSummary;
import kz.aitu.real_archive_project.service.NomenclatureSummaryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NomenclatureSummaryController {
    private final NomenclatureSummaryService nomenclatureSummaryService;

    public NomenclatureSummaryController(NomenclatureSummaryService nomenclatureSummaryService) {
        this.nomenclatureSummaryService = nomenclatureSummaryService;
    }

    @GetMapping("api/nomenclature_summary/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(nomenclatureSummaryService.getById(id));
    }

    @GetMapping("api/nomenclature_summary")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(nomenclatureSummaryService.getAll());
    }

    @DeleteMapping("api/nomenclature_summary/{id}")
    public void deleteById(@PathVariable long id){
        nomenclatureSummaryService.deleteById(id);
    }

    @PostMapping("api/nomenclature_summary")
    public ResponseEntity<?> createAuth(@RequestBody NomenclatureSummary nomenclatureSummary){
        return ResponseEntity.ok(nomenclatureSummaryService.createNomenclatureSummary(nomenclatureSummary));
    }
}