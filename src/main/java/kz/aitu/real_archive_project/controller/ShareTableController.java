package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.RequestTable;
import kz.aitu.real_archive_project.model.ShareTable;
import kz.aitu.real_archive_project.service.ShareTableService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ShareTableController {
    private ShareTableService shareTableService;

    public ShareTableController(ShareTableService shareTableService) {
        this.shareTableService = shareTableService;
    }

    @GetMapping("api/share_table/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(shareTableService.getById(id));
    }

    @GetMapping("api/share_table")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(shareTableService.getAll());
    }

    @DeleteMapping("api/share_table/{id}")
    public void deleteById(@PathVariable long id){
        shareTableService.deleteById(id);
    }

    @PostMapping("api/share_table")
    public ResponseEntity<?> createAuth(@RequestBody ShareTable shareTable){
        return ResponseEntity.ok(shareTableService.createShareTable(shareTable));
    }
}
