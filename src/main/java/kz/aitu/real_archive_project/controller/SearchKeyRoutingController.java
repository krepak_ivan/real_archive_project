package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.SearchKeyRouting;
import kz.aitu.real_archive_project.model.Searchkey;
import kz.aitu.real_archive_project.service.SearchKeyRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchKeyRoutingController {
    private final SearchKeyRoutingService searchKeyRoutingService;

    public SearchKeyRoutingController(SearchKeyRoutingService searchKeyRoutingService) {
        this.searchKeyRoutingService = searchKeyRoutingService;
    }

    @GetMapping("api/search_key_routing/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(searchKeyRoutingService.getById(id));
    }

    @GetMapping("api/search_key_routing")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(searchKeyRoutingService.getAll());
    }

    @DeleteMapping("api/search_key_routing/{id}")
    public void deleteById(@PathVariable long id){
        searchKeyRoutingService.deleteById(id);
    }

    @PostMapping("api/search_key_routing")
    public ResponseEntity<?> createAuth(@RequestBody SearchKeyRouting searchKeyRouting){
        return ResponseEntity.ok(searchKeyRoutingService.createSearchKeyRouting(searchKeyRouting));
    }
}