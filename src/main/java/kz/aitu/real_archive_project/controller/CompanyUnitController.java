package kz.aitu.real_archive_project.controller;

import kz.aitu.real_archive_project.model.ActivityJournal;
import kz.aitu.real_archive_project.model.CompanyUnit;
import kz.aitu.real_archive_project.service.CompanyUnitService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyUnitController {
    private final CompanyUnitService companyUnitService;

    public CompanyUnitController(CompanyUnitService companyUnitService) {
        this.companyUnitService = companyUnitService;
    }

    @GetMapping("api/company_unit/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(companyUnitService.getById(id));
    }

    @GetMapping("api/company_unit")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(companyUnitService.getAll());
    }

    @DeleteMapping("api/company_unit/{id}")
    public void deleteById(@PathVariable long id){
        companyUnitService.deleteById(id);
    }

    @PostMapping("api/company_unit")
    public ResponseEntity<?> createAuth(@RequestBody CompanyUnit companyUnit){
        return ResponseEntity.ok(companyUnitService.createCompanyUnit(companyUnit));
    }
}