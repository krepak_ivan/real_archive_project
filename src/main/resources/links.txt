1) authorization_table
http://127.0.0.1:7771/api/authorization_table
http://127.0.0.1:7771/api/authorization_table/{id}

2) request_table
http://127.0.0.1:7771/api/request_table
http://127.0.0.1:7771/api/request_table/{id}

3) share_table
http://127.0.0.1:7771/api/share_table
http://127.0.0.1:7771/api/share_table/{id}

4) history_of_request_statuses
http://127.0.0.1:7771/api/history_of_request_statuses
http://127.0.0.1:7771/api/history_of_request_statuses/{id}

5) activity_journal
http://127.0.0.1:7771/api/activity_journal
http://127.0.0.1:7771/api/activity_journal/{id}

6) notification
http://127.0.0.1:7771/api/notification
http://127.0.0.1:7771/api/notification/{id}

7) fond
http://127.0.0.1:7771/api/fond
http://127.0.0.1:7771/api/fond/{id}

8) company
http://127.0.0.1:7771/api/company
http://127.0.0.1:7771/api/company/{id}

9) company_unit
http://127.0.0.1:7771/api/company_unit
http://127.0.0.1:7771/api/company_unit/{id}

10) users
http://127.0.0.1:7771/api/users
http://127.0.0.1:7771/api/users/{id}

11) catalog
http://127.0.0.1:7771/api/catalog
http://127.0.0.1:7771/api/catalog/{id}

12) catalog_case
http://127.0.0.1:7771/api/catalog_case
http://127.0.0.1:7771/api/catalog_case/{id}

13) location
http://127.0.0.1:7771/api/location
http://127.0.0.1:7771/api/location/{id}

14) tempfiles
http://127.0.0.1:7771/api/tempfiles
http://127.0.0.1:7771/api/tempfiles/{id}

15) file
http://127.0.0.1:7771/api/file
http://127.0.0.1:7771/api/file/{id}

16) case_table
http://127.0.0.1:7771/api/case_table
http://127.0.0.1:7771/api/case_table/{id}

17) file_routing
http://127.0.0.1:7771/api/file_routing
http://127.0.0.1:7771/api/file_routing/{id}

18) delete_document
http://127.0.0.1:7771/api/delete_document
http://127.0.0.1:7771/api/delete_document/{id}

19) nomenclature_summary
http://127.0.0.1:7771/api/nomenclature_summary
http://127.0.0.1:7771/api/nomenclature_summary/{id}

20) nomenclature
http://127.0.0.1:7771/api/nomenclature
http://127.0.0.1:7771/api/nomenclature/{id}

21) case_index
http://127.0.0.1:7771/api/case_index
http://127.0.0.1:7771/api/case_index/{id}

22) record
http://127.0.0.1:7771/api/record
http://127.0.0.1:7771/api/record/{id}

23) searchkey
http://127.0.0.1:7771/api/searchkey
http://127.0.0.1:7771/api/searchkey/{id}

24) search_key_routing
http://127.0.0.1:7771/api/search_key_routing
http://127.0.0.1:7771/api/search_key_routing/{id}