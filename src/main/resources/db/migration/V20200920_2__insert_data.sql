insert into authorization_table(id, username, email, password, role, forgot_password_key, forgot_password_timestamp, company_unit_id)
values
(1, 't_aubakirov', 't_aubakirov@mail.ru', 'JPKVhrPcMBNGm8PW', 'Pilot', 'cFQ8aVTaqFzeHLTX', 20191208, 909),
(2, 'a_toktar', 'a_toktar@mail.kz', 'wjkkdL6N7mGgqaDz', 'Astronaut', 'Ujjv3Tu3QGCAe2Ds', 19730710, 907),
(3, 'a_ayimbetov', 'a_ayimbetov@rambler.ru', 'hgNGKR8fCxHef3BS', 'Command pilot', 'UdjtWBUn2ZUBFdXs', 19800510, 904),
(4, 'a_aydyn', 'a_aydyn@gmail.com', 'pR7kd2Q5wzuFZd84', 'Commander', '5fk25FrucNFudnpJ', 19720706, 901),
(5, 't_musabayev', 't_musabayev@aol.com', 'ySQ6ThPLdzrkqPFa', 'Command module pilot', 'V98sPHNnXYRwEMK4', 19730303, 903),
(6, 'm_talgat', 'm_talgat@yahoo.com', 'z9WxtbvUUB2gYWqj', 'Lunar module pilot', 'DaEFzCs67rU3cc8v', 20171104, 908),
(7, 'y_lonchakov', 'y_lonchakov@nis.edu.kz', 'ubaHUGnhLcWz9Aej', 'Docking module pilot', 'LfYckZGMh5pNRLjk', 19630309, 906),
(8, 'l_yuri', 'l_yuri@fmalm.nis.edu.kz', 'V4wraw2928A4Lnsd', 'Science pilot', 'Xb2Q5F5JRzdhYHCt', 19960302, 902),
(9, 'a_serikov', 'a_serikov@semey@nis.edu.kz', 'hsjVsVkrssCLppwR', 'Payload commander', 'Ypwt3peUCLAUAE34', 19740201, 910),
(10, 's_ayan', 's_ayan@ib.nis.edu.kz', 'bMwUKj62DL4PCxvs', 'Mission specialist', 'Ypwt3peUCLAUAE34', 20190728, 905),
(11, 'y_gagarin', 'y_gagarin@nisa.edu.kz', 'R6gdz5Vfyr3psfTa', 'Flight engineer', 'dhXXgMsgFWkSEry6', 19820901, 906);

insert into request_table(id, request_user_id, response_user_id, case_index_id, created_type, comment, status, timestamp, sharetimestamp, sharefinish, favorite,
                          update_timestamp, update_by, declinenote, company_unit_id, from_request_id)
values
(101, 1005, 1004, 1601, 'Spaceflight participant', 'Commander', 'Mercury Seven', 19681224, 19881108, 19950302, true, 19750711, 20120508, 'Second man on the Moon',
 20001215, 20020730),
(102, 1001, 1003, 1602, 'SpaceX crew dragon', 'Flight engineer', 'James McDivitt', 19790907, 19620831, 19641106, false, 19800105, 19611109, 'Backup LMP', 19830913,
 20020921),
(103, 1006, 1008, 1606, 'Spacecraft commander', 'Spaceflight participant', 'Ed White', 19711012, 19880222, 20140512, true, 19611110, 20130505, 'Fred Haise',
 19610218, 20190315),
(104, 1010, 1006, 1603, 'Joint operations commander', 'Vostok', 'Neil Armstrong', 19850404, 20040301, 19950803, false, 19821119, 19620527, 'Deke Slayton', 19920129,
 19840611),
(105, 1007, 1001, 1608, 'Pilot', 'Voskhod', 'Jim Lovell', 20140721, 19940530, 19781106, false, 20081021, 19831022, 'Mercury 7 astronaut', 19790812, 19950416),
(106, 1003, 1010, 1605, 'Mission specialist', 'Soyuz', 'Backup commander', 19880405, 20080609, 20110508, true, 20121126, 19901115, 'Pete Conrad', 20030320,
 19931204),
(107, 1004, 1007, 1610, 'Pilot sosmonaut', 'Commander', 'Apollo 13', 20001019, 19940901, 19741010, true, 20031130, 19850710, 'First Skylab commander',
 19950727, 19980102),
(108, 1002, 1005, 1604, 'Commander', 'Flight engineer', 'Michael Collins', 19811222, 20020325, 19810516, false, 20080703, 19860422, 'Paul J. Weitz', 20050125,
 19930404),
(109, 1009, 1001, 1609, 'Second pilot', 'Science officer', 'Backup CMP', 19821120, 19970815, 20060330, true, 20180428, 20150408, 'Joseph P. Kerwin',
 19870312, 19610327),
(110, 1008, 1010, 1607, 'Scientist cosmonaut', 'Spaceflight participant', 'William Anders', 19770527, 19970710, 20161105, false, 19621203, 19940801,
 'first American physician in space', 20030414, 19850227),
(111, 1009, 1003, 1604, 'Doctor cosmonaut', 'Overall mission success', 'Buzz Aldrin', 20061106, 19700523, 19870924, true, 19970202, 19730620,
 'John Young', 19910517, 20160105);

insert into share_table (id, request_id, note, sender_id, receiver_id, share_timestamp)
values
(301, 102, 'Commander of the first Shuttle mission', 1001, 1009, 19881109),
(302, 109, 'Robert Crippen', 1006, 1007, 19770919),
(303, 104, 'Flew the first Space Shuttle mission as pilot', 1004, 1005, 20200603),
(304, 103, 'Story Musgrave', 1003, 1002, 20070827),
(305, 110, 'Michael P. Anderson', 1010, 1008, 20120804),
(306, 106, 'Jerry L. Ross', 1010, 1009, 19610323),
(307, 107, 'Franklin Chang-Diaz', 1008, 1007, 20161222),
(308, 105, 'Each flew seven times as shuttle mission specialists', 1001, 1004, 19901028),
(309, 101, 'Story Musgrave', 1006, 1002, 19721205),
(310, 108, 'Sally Ride', 1003, 1005, 20071111),
(311, 103, 'Michael P. Anderson', 1010, 1007, 19800711);

insert into history_of_request_statuses (id, request_id, status, created_timestamp, created_by, updated_timestamp, updated_by)
values
(401, 108, 'Hans Schlegel', 19800711, 1008, 19710706, 1010),
(402, 107, 'Joseph M. Acaba', 19930630, 1004, 20021228, 1006),
(403, 110, 'First Puerto Rican astronaut', 20100702, 1009, 19990623, 1007),
(404, 102, 'Byron K. Lichtenberg', 19760214, 1005, 20180903, 1001),
(405, 106, 'First payload specialist', 19950309, 1002, 20060823, 1003),
(406, 103, 'Ulf Merbold', 19830507, 1005, 20071013, 1001),
(407, 101, 'First international payload specialist', 19610604, 1003, 20160709, 1002),
(408, 109, 'Charles Walker', 20020309, 1009, 19920328, 1006),
(409, 104, 'Flew three times', 20001209, 1008, 20110919, 1004),
(410, 105, 'Ilan Ramon', 20030107, 1007, 19780505, 1010),
(411, 106, 'Last payload specialist', 19810725, 1010, 19661025, 1003);

insert into activity_journal(id, type_of_event, object_type, object_id, created_timestamp, created_by, message_level, message)
values
(501, 'Gary Payton', 'First Crew dragon mission specialist', 180, 19991113, 1002, 'McDivitt', 'Position'),
(502, 'Christa McAuliffe', 'Single-seat spacecraft', 180, 20160603, 1006, 'Command pilot', 'Apollo–Soyuz'),
(503, 'Teacher in space', 'Astronauts', 82, 20120405, 1005, 'First American', 'Joint mission'),
(504, 'Space shuttle challenger disaster', 'Mercury missions', 21, 19631213, 1004, 'EVA', 'Shuttle commanders'),
(505, 'Douglas G. Hurley', 'Pilots', 5, 19780808, 1007, 'Extravehicular activity', 'Prior spaceflight experience'),
(506, 'Demo-2', 'Mercury pilots', 16, 19940913, 1009, 'Aldrin', 'Degree in engineering'),
(507, 'Robert L. Behnken', 'Experience', 20, 20020616, 1001, 'Doctor of philosophy', 'Biological science'),
(508, 'Demo-2', 'Pilot', 30, 20060920, 1008, 'Technically', 'Physical science'),
(509, 'Victor Glover', 'High-performance', 7, 19930710, 1003, 'Doctor of Science', 'Mathematics'),
(510, 'USCV-1', 'Jet aircraft', 18, 19900108, 1010, 'Sc.D.', 'Flying experience'),
(511, 'Soichi Noguchi', 'To be no more than 5 feet 11 inches', 20, 19900511, 1002, 'Space', 'Jet aircraft');

insert into notification(id, object_type, object_id, company_unit_id, user_id, created_timestamp, veiwed_timestamp, is_viewed, title, text_of_notification,
                         company_id)
values
(601, '750 simulated landings', 93, 906, 1004, 20190329, 20120527, true, 'NASA astronauts', 'Saudi Arabia', 805),
(602, 'Shuttle training aircraft', 46, 907, 1002, 19780617, 20041213, true, 'NASA class II', 'Mexico', 810),
(603, 'NASA class I', 27, 903, 1003, 20150312, 19990802, false, 'Space physical', 'Space Shuttle', 801),
(604, 'Space physical', 80, 910, 1006, 19710119, 20100418, true, 'Flight', 'Payton', 806),
(605, 'Flight', 56, 901, 1001, 19700322, 19790223, true, 'FE', 'William A. Pailes', 808),
(606, 'Education', 54, 904, 1005, 20080902, 19971216, true, 'Mission specialist 2', 'Manned spaceflight engineers', 802),
(607, 'Flight experience', 52, 909, 1007, 19920105, 20050803, false, 'S4 seat', 'Tourists and other special travelers from the career astronauts', 803),
(608, 'Requirements', 94, 902, 1010, 20111111, 20031016, false, 'Shuttle flight deck', 'Demo-2 mission', 807),
(609, 'Commander', 40, 908, 1008, 19780405, 20030222, true, 'Educator astronaut project', 'NASA', 804),
(610, 'Prior spaceflight experience', 57, 905, 1009, 19650507, 19880412, true, 'Payload Specialists', 'Crew dragon flights', 809),
(611, 'Payload сommanders', 50, 902, 1001, 19920522, 19800409, true, 'Non-NASA personnel', 'USCV-1', 806);

insert into fond(id, fond_number, created_timestamp, create_by, updated_timestamp, updated_by)
values
(701, '19780624', 19770824, 19701226, 19730121, 19920106),
(702, '19990304', 20151012, 19880317, 19881230, 20050930),
(703, '20180418', 19780321, 19710906, 19700901, 19810611),
(704, '19610117', 20080323, 19820117, 19701005, 19610727),
(705, '19980822', 20160804, 20140424, 19741005, 19850425),
(706, '19630403', 20191111, 20111130, 19630612, 19610323),
(707, '19831125', 20150506, 19620529, 19920523, 20010310),
(708, '20031203', 19981020, 19610627, 20191110, 19811102),
(709, '20130608', 19610828, 19740617, 19860417, 19780816),
(710, '19760727', 19840623, 20170319, 20060513, 19930628),
(711, '19710508', 20100806, 19660601, 20071026, 19680701);

insert into company(id, name_ru, name_kz, name_en, bin, parent_id, fond_id, created_timestamp, created_by, updated_timestamp, updated_by)
values
(801, 'Kosmonavt', 'Garyshker', 'Astronaut', 199903041976, 19791212, 702, 19990528, 19630124, 19830517, 19910311),
(802, 'Kosmos', 'Garysh', 'Space', 198012150519, 19640621, 706, 20100824, 19960714, 19670902, 20111105),
(803, 'Raketa', 'Zymyran', 'Rocket', 200512242014, 19730714, 707, 20200815, 20070708, 19810108, 19880504),
(804, 'Letchik', 'Ushkush', 'Pilot', 199811171215, 19911223, 703, 19900816, 19970128, 20151224, 19760716),
(805, 'Samolet', 'Ushaq', 'Airplane', 202006101961, 20141205, 704, 20140915, 19790809, 19980524, 19770722),
(806, 'Sputnik', 'Serik', 'Satellite', 201001300320, 20010405, 708, 19770909, 19630102, 19960401, 19871110),
(807, 'Raketanositel', 'Tasygysh-zymyran', 'Rocket carrier', 201803090228, 19870507, 710, 19770418, 20120116, 19970703, 20110403),
(808, 'Startovaya ploshadka', 'Sore alany', 'Platform', 196402042006, 19620810, 705, 19660101, 20060206, 20040221, 20090819),
(809, 'Kosmodrom', 'Garyshzhay', 'Cosmodrome', 197701230311, 19861007, 701, 19730711, 19790125, 19890308, 20051205),
(810, 'Planeta', 'Galamshar', 'Planet', 197404091983, 19900322, 709, 19971024, 19751008, 20061228, 20050602),
(811, 'Vselennaya', 'Bukil alem', 'Universe', 201603230213, 19951227, 704, 20121211, 19660528, 19970118, 20140802);

insert into company_unit(id, name_ru, name_kz, name_en, parent_id, year, company_id, code_index, created_timestamp, created_by, updated_timestamp, updated_by)
values
(901, 'Activ', 'Activ', 'Activ', 19781031, 1965, 802, 11260911, 20021214, 20010531, 20160324, 19900608),
(902, 'Tele Dva', 'Tele Eki', 'Tele Two', 19680204, 1983, 806, 19920721, 20191129, 20190111, 19950227, 19740107),
(903, 'Beeline', 'Beeline', 'Beeline', 19650616, 1968, 809, 19630528, 19860807, 20080303, 20001010, 19831221),
(904, 'Altel', 'Altel', 'Altel', 20050707, 1990, 810, 19691207, 20100306, 19920301, 19740622, 20140418),
(905, 'Kcell', 'Kcell', 'Kcell', 19940821, 1995, 807, 20121109, 19850816, 19931203, 19850718, 19840809),
(906, 'MTS', 'MTS', 'MTS', 19820415, 2006, 803, 20110902, 20041214, 20140928, 20110506, 20101002),
(907, 'Megafon', 'Megafon', 'Megafon', 19940306, 2010, 808, 19751112, 19610508, 19960103, 19941209, 19970124),
(908, 'Yota', 'Yota', 'Yota', 19780705, 2002, 804, 19990706, 19770616, 19790430, 19821122, 19650825),
(909, 'Rostelecom', 'Rostelecom', 'Rostelecom', 19820112, 1992, 801, 19610205, 20080526, 19621106, 19620305, 20070219),
(910, 'Megacom', 'Megacom', 'Megacom', 20160312, 1995, 808, 19610101, 19780805, 19771223, 20141115, 19770818),
(911, 'OSign', 'OSign', 'OSign', 19790923, 2004, 805, 20020530, 20140830, 19971022, 19971101, 20100927);

insert into users(id, auth_id, name, fullname, surname, secondname, status, company_unit_id, password, last_login_timestamp, iin, is_active, is_activated,
                  created_timestamp, created_by, updated_timestamp, updated_by)
values
(1001, 20151105, 'Yuri', 'Yuri Gagarin', 'Gagarin', 'Gagarin', 'Peggy Whitson', 902, '8bchsTG2KpHEZ26K', 19821005, 199501230529, true, false,
 19950622, 199909122004, 19631227, 198607240427),
(1002, 19710228, 'Vladimir', 'Vladimir Komarov', 'Komarov', 'Komarov', 'Robert Thirsk', 909, 'eXbw2C4ycU5XYddd', 19911027, 201203152002,
 true, false, 20060505, 201504172006, 19640713, 198611161201),
(1003, 20010906, 'Alexei', 'Alexei Leonov', 'Leonov', 'Leonov', 'First Canadian astronaut', 903, 'mtcVUw43ny8uSJBt', 20180511, 197902210717,
 false, true, 20050316, 198711301963, 20050218, 201808080822),
(1004, 19990518, 'Konstantin', 'Konstantin Feoktistov', 'Feoktistov', 'Feoktistov', 'ISS expedition', 904, 'umsBAqzzbfqg3E7D', 19700329,
 197903141980, true, true, 20121001, 200802071972, 19770519, 200508260729),
(1005, 19980607, 'Boris', 'Boris Yegorov', 'Yegorov', 'Yegorov', 'First science officer', 907, 'Y4sdVwkb24rVu7Xc', 19760723, 197601120422,
 false, false, 19810605, 201310011991, 20100721, 202004111229),
(1006, 19810805, 'Vladimir', 'Vladimir Dzhanibekov', 'Dzhanibekov', 'Dzhanibekov', 'Anousheh Ansari', 905, 'EyTMXdkaZKr6tqta', 19920715, 197704101993,
 false, false, 19760120, 197210012015, 19900428, 197306200802),
(1007, 20001029, 'Salyut', 'Salyut 6', 'Salyut', 'Salyut', 'Single-seat spacecraft', 906, 'yW2UcMCWqyk482k6', 19810304, 197707110223, false, false,
 19901028, 200006052009, 19960307, 201309278030),
(1008, 19970612, 'Salyut', 'Salyut 7', 'Salyut', 'Salyut', 'Vostok', 908, 'ndBBBRLCyzMk3YXK', 19871121, 200308301965, true, true,
 20200705, 197406171985, 20070219, 201303071025),
(1009, 19771008, 'Svetlana', 'Svetlana Savitskaya', 'Savitskaya', 'Savitskaya', 'Pilot cosmonauts', 901, '2VtRT6N8Ujc65SQM', 19890130, 199908070329, true,
 true, 19920309, 196101151962, 19800921, 197408240306),
(1010, 19921114, 'Muszaphar', 'Sheikh Muszaphar Shukor', 'Shukor', 'Sheikh', 'Soyuz', 910, 'dXmAXJn9phNbDQb2', 20041214, 201710302017,
 true, true, 20050305, 196603172015, 19610404, 199607020527),
(1011, 20161116, 'Dennis', 'Dennis Tito', 'Tito', 'Tito', 'ISS', 907, 'aAcg6udqwANVsykm', 19881011, 199607100204, true, false,
 19951207, 198502221964, 19860102, 197911111020);

insert into catalog(id, name_ru, name_kz, name_en, parent_id, company_unit_id, created_timestamp, created_by, updated_timestammp, updated_by)
values
(1101, 'Ganvest', 'Ganvest', 'Ganvest', 20020912, 906, 19780725, 198307101109, 19910727, 198907221999),
(1102, 'Skan', 'Skan', 'Skan', 20031102, 902, 19681105, 199512050729, 19690112, 196306231978),
(1103, 'Swisha', 'Swisha', 'Swisha', 19950222, 909, 20070330, 201303241122, 20200703, 200108312015),
(1104, 'KDK', 'KDK', 'KDK', 19960828, 910, 19750410, 197306140105, 20190819, 199510161976),
(1105, 'Illa', 'Illa', 'Illa', 20090408, 903, 19690828, 199112180330, 20190129, 198501110408),
(1106, 'Blackowl', 'Blackowl', 'Blackowl', 19992145, 905, 19630922, 198312161985, 19961130, 201308290702),
(1107, 'Mater', 'Mater', 'Mater', 19911008, 908, 19870423, 197101151995, 19941114, 200709212018),
(1108, 'Brownie', 'Brownie', 'Brownie', 19930823, 904, 20051208, 196507020425, 19941118, 200104162006),
(1109, 'Proovy', 'Proovy', 'Proovy', 19921003, 907, 20120919, 202002190806, 20091122, 200105111986),
(1110, 'Flesh', 'Flesh', 'Flesh', 19741112, 901, 20040607, 196205020606, 20190516, 201105202020),
(1111, 'Kloukoma', 'Kloukoma', 'Kloukoma', 19890306, 906, 20180101, 200204260920, 20000722, 196512191998);

insert into catalog_case(id, case_id, catalog_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
values
(1201, 19810629, 1105, 906, 19760107, 196706210107, 19650315, 201312092010),
(1202, 19960409, 1107, 902, 20190920, 201401180624, 19740120, 199007071998),
(1203, 19750630, 1102, 905, 20060723, 199211260326, 19760714, 199904032003),
(1204, 19840314, 1109, 901, 19870925, 201003300530, 19611207, 197306051972),
(1205, 19760305, 1108, 904, 20190517, 196901100725, 19961124, 199003021985),
(1206, 20180103, 1103, 910, 19810106, 196804040618, 20010914, 202001040214),
(1207, 19920226, 1110, 903, 19871105, 200705052014, 20100501, 197908171987),
(1208, 19710920, 1102, 907, 20040412, 198201150502, 19910307, 198812081981),
(1209, 19680201, 1106, 908, 19801002, 198901271024, 19751018, 200709241987),
(1210, 19890810, 1104, 909, 20180126, 200104121115, 19971019, 199712201988),
(1211, 19790916, 1101, 906, 19730919, 196404250616, 19640110, 196310281976);

insert into location(id, row, line, column_location, box, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
values
(1301, 'First', 'First', 'Low', 'Red', 905, 19920623, 196310310605, 19711022, 198703251978),
(1302, 'Second', 'Second', 'Middle', 'Green', 909, 19720728, 197007260324, 20110903, 199102142011),
(1303, 'Third', 'Third', 'High', 'Blue', 908, 19810103, 201706200103, 20020404, 197907162010),
(1304, 'First', 'Forth', 'Low', 'Red', 904, 19690513, 201604160426, 19721209, 200707181970),
(1305, 'Second', 'First', 'Middle', 'Green', 902, 20121215, 197109070525, 20040405, 196601032006),
(1306, 'Third', 'Second', 'High', 'Blue', 910, 20070701, 198910210109, 19870605, 196401271994),
(1307, 'First', 'Third', 'Low', 'Red', 907, 19990409, 200408021211, 19960626, 196906121978),
(1308, 'Second', 'Forth', 'Middle', 'Green', 901, 20090428, 199109261985, 20050317, 199505210302),
(1309, 'Third', 'First', 'High', 'Blue', 903, 19610719, 201304130324, 19671214, 201512261963),
(1310, 'First', 'Second', 'Low', 'Red', 906, 19951209, 200608031994, 20190113, 201909131007),
(1311, 'Second', 'Third', 'Middle', 'Green', 905, 19650503, 198901161215, 19880324, 196112041998);

insert into tempfiles(id, file_binary, file_binary_byte)
values
(1401, 'File 1 binary', 'File 1 binary byte'),
(1402, 'File 2 binary', 'File 2 binary byte'),
(1403, 'File 3 binary', 'File 3 binary byte'),
(1404, 'File 4 binary', 'File 4 binary byte'),
(1405, 'File 5 binary', 'File 5 binary byte'),
(1406, 'File 6 binary', 'File 6 binary byte'),
(1407, 'File 7 binary', 'File 7 binary byte'),
(1408, 'File 8 binary', 'File 8 binary byte'),
(1409, 'File 9 binary', 'File 9 binary byte'),
(1410, 'File 10 binary', 'File 10 binary byte'),
(1411, 'File 11 binary', 'File 11 binary byte');

insert into file(id, name, type, size, page_count, hash, is_deleted, file_binary_id, created_timestamp, created_by, updated_timestamp, updated_by)
values
(1501, 'Chrome.exe', 'Browser', 19781022, 19, 20181123820, true, 1408, 19670203, 198401281976, 20120507, 199205171997),
(1502, 'Chrome.dmg', 'Browser', 19730821, 63, 19790706211, false, 1409, 20140618, 199506021975, 19721120, 199410240116),
(1503, 'Opera.exe', 'Browser', 20171017, 405, 198312060131, true, 1406, 19950822, 201102250519, 19810927, 201605121967),
(1504, 'Opera.dmg', 'Browser', 19691026, 19, 197301152003, true, 1401, 19781025, 200201070405, 19610320, 197005191993),
(1505, 'Chromium.exe', 'Browser', 19750507, 84, 196802180805, true, 1404, 20170406, 201712051983, 19810814, 198308260825),
(1506, 'Chromium.dmg', 'Browser', 19720622, 11, 197009292013, false, 1405, 19780917, 196904150404, 20151223, 200403241998),
(1507, 'Telegram.exe', 'Messenger', 19611127, 21, 201503080807, true, 1403, 19780513, 201010171009, 19790418, 197806132020),
(1508, 'Telegram.dmg', 'Messenger', 19701107, 19, 196302202015, false, 1402, 19811203, 201111132012, 19710119, 201003131115),
(1509, 'Evernote.exe', 'Notes', 19721109, 87, 199403190513, false, 1407, 19670924, 196304220217, 19760531, 197612062005),
(1510, 'Evernote.dmg', 'Notes', 19771123, 105, 200101131971, false, 1410, 19850622, 200610271974, 20091219, 200801091231),
(1511, 'Brackets.exe', 'HTML editor', 20061221, 19, 196402220325, true, 1408, 19850720, 200101240905, 20051230, 199610161979);

insert into case_table(id, case_number, folder_number, case_title_ru, case_title_kz, case_title_en, start_date, end_date, pages_amount, ecp_flag,
                       ecp_text, naf_send, deletion_flag, restricted_access_flag, hash, version, id_version, is_active_flag, comment, id_location,
                       id_index, id_deletion_document, structure_unit_id, blockchain_address, adding_to_blockchain_date, creating_date, case_author,
                       case_editing, case_editing_author)
values
(1601, '196812050404', '199708022006', 'ISS', 'Astronauts', 'NASA', 1991021, 19801016, 19, false, '199308230612', false, false, false, '006103009013',
 1988, '200010031229', false, 'Astronauts', 1307, 199707220811, 197803151994, 910, '202008210817', 19750325, 19680831, 200419930718,
 197710290503, 199120181108),
(1602, '199210052019',  '198902160625', 'Astronaut groups', 'Joe Engle', 'STS-2', 19791005, 19631122, 66, false, '200406021974', false, false, false,
 '198909211982', 1987, '196309292016', false, 'Career', 1304, 201703251990, 199004041212, 901, '197902041991', 19871015, 19880326, 196212021014,
 197419640522, 200207121211),
(1603, '200101130825',  '198508161987', 'NASA', 'X-15', 'Shuttle approach', 20100211, 20050202, 731, true, '200502180710', false, true, false, '200712050601',
 2008, '198103060718', false, 'Education', 1308, 197208071215, 201305132006, 904, '199702231006', 19861128, 19981203, 197206261970, 201001140128, 200002181963),
(1604, '202008112001',  '201102160110', 'Landing tests', 'Astronauts', ' Career', 20151206, 19850526, 19, false, '200001182000', true, false, false, '197503192002',
 1974, '201408181963', false, 'Pilot', 1309, 196805011974, 198703300616, 905, '197203051999', 19861206, 20180323, 200505081107, 198212032011, 200207300907),
(1605, '198403180115',  '201701252005', 'Education', 'Pilot', 'Commander', 19840827, 20190811, 80, true, '201811210302', false, false, false, '196904060622',
 1988, '200007010811', false, 'Commander', 1303, 199904150721, 198206131969, 903, '200709101204', 20180211, 19990718, 201020190921, 201012200901, 200701031964),
(1606, '196608042011',  '199305280823', 'Mission specialist', 'NASA', 'Space shuttle', 19710604, 19680730, 112, true, '197710211999', true, true, true,
 '199004161975', 1970, '197003272005', true, 'Mission specialist', 1310, 196705251968, 200407280207, 902, '198109052009', 19940731, 19860904, 196105150919,
 197512302008, 199601060313),
(1607, '197011260116',  '200907291966', 'Flight', 'Kidzworld', 'Bolden', 20140810, 19820628, 19, false, '199409090422', false, true, false, '199407090815',
 2007, '201002121120', false, 'NASA', 1301, 196905100517, 197204162003, 908, '198110020715', 19610425, 19890827, 201904211968, 200101080925, 198704191977),
(1608, '198912271991',  '196110250508', 'Charles F', 'Bolden', 'NASA', 19731217, 20020826, 91, false, '199601181992', false, true, true, '197903241969',
 2006, '201911021994', false, 'Space shuttle', 1302, 197608292016, 199706080531, 909, '197102082005', 20161231, 19860719, 201302202018, 200907240413, 201309080428),
(1609, '200712070129',  '197507012017', 'Johnson space center', 'Oral history project', 'Interview', 19801029, 19920315, 116, false, '198812280105', false,
 true, false, '197007150219', 2007, '196203301229', false, 'Flight', 1306, 197304070106, 198604291974, 906, '198612250722', 19730812, 20000130, 200901171987,
 198101141968, 201804121206),
(1610, '201008162018',  '200208090818', 'Johnson', 'Sandra', 'Wright', 19780621, 19660715, 20, false, '200505261981', true, false, false, '196909222002',
 1968, '196903171967', false, 'NASA', 1305, 196403201981, 196202201105, 907, '198720120317', 20200813, 19970217, 200305080424, 197711051999, 199008040824),
(1611, '201801281002',  '198002212007', 'Rebecca', 'Ross-Nazzal', 'Jennifer. Houston', 19641230, 19800215, 19, true, '197503181013', true, false, false,
 '199002101023', 1979, '197507290101', false, 'Space shuttle Columbia', 1307, 201809050311, 199303201979, 910, '198502060225', 20141216, 19960128, 198808271965,
 201609101111, 197211222013);

insert into file_routing(id, file_id, table_name, table_id, type)
values
(1701, 1505, 'Launch America', 19850705, 'Soviet space program'),
(1702, 1507, 'NASA', 19750905, 'Roscosmos'),
(1703, 1504, 'SpaceX', 20010808, 'Soyuz 1'),
(1704, 1503, 'Space station', 19921102, 'Baikonur'),
(1705, 1509, 'National Aeronautics and Space Administration', 20050820, 'Capsule'),
(1706, 1502, 'McHale', 20090409, 'Soyuz'),
(1707, 1508, 'Suzy', 19810629, 'Soyuz-U'),
(1708, 1510, 'RuSpace', 20130528, 'Soyuz-FG'),
(1709, 1501, 'Apollo–soyuz test project', 19781110, 'Soyuz-2'),
(1710, 1506, 'Soviet Union', 20000607, 'Kosmos 133'),
(1711, 1505, 'Russia', 19690304, 'Kosmos 140');

insert into delete_document(id, document_number, reason, company_unit_id, creation_date, author, edition_date, edition_author)
values
(1801, '196707010306', 'Kosmos 186', 910, 20000604, 200707022015, 20010413, 196905141119),
(1802, '196104040328', 'Kosmos 188', 906, 19710625, 196206191985, 19840814, 197208150312),
(1803, '197720110102', 'Kosmos 212', 907, 20020521, 200309241975, 19770326, 196808291217),
(1804, '199819951124', 'Kosmos 213', 903, 20200415, 199511211980, 19970518, 199702060314),
(1805, '198608120319', 'Kosmos 238', 902, 19930912, 198107131996, 19800320, 201312070714),
(1806, '199020190410', 'Soyuz 2', 905, 20090602, 199108181978, 19971218, 196109110102),
(1807, '197006111003', 'Kosmos 379', 908, 19910805, 196901301019, 20071011, 199006061978),
(1808, '196319780818', 'Kosmos 396', 904, 20020511, 198712151008, 20191117, 196409062013),
(1809, '198709110514', 'Kosmos 434', 909, 20110908, 197702171989, 19820323, 199805131112),
(1810, '198820051106', 'Kosmos 496', 901, 19930426, 196411231976, 19650602, 197604100514),
(1811, '196808081211', 'Kosmos 573', 910, 20030701, 200601252019, 19661001, 196107030907);

insert into nomenclature_summary(id, number_of_nomenclature_summary, year, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
values
(1901, '083101150913', 2017, 906, 19630730, 197312151224, 19760708, 196703092004),
(1902, '072020080610', 1972, 905, 19700617, 196403060222, 20040216, 198004142003),
(1903, '010102180527', 1975, 901, 20060705, 199204270403, 19610730, 198701131997),
(1904, '121612140602', 1998, 904, 20040705, 201801202002, 20130619, 199811220920),
(1905, '060101202020', 1990, 903, 19790205, 201409130802, 20060617, 196602191969),
(1906, '061520170930', 1971, 907, 20000110, 196809031992, 19640925, 197604050101),
(1907, '199501272012', 1976, 909, 19881218, 200807151219, 19970503, 201107232018),
(1908, '082819950806', 1985, 910, 20130307, 198405031986, 19970921, 199312270714),
(1909, '201407101976', 1967, 902, 20060319, 196402100720, 19610901, 201611202015),
(1910, '197004030901', 2004, 908, 19860426, 198904161968, 19651128, 196710271971),
(1911, '201409051961', 2014, 906, 19630611, 198004180901, 19971005, 199401300521);

insert into nomenclature(id, nomenclature_number, year, nomenclature_summary_id, company_unit_id, created_timestamp, created_by, updated_timestamp,
                         updated_by)
values
(2001, '198305100612', 2020, 1907, 906, 19990729, 040119930404, 19680121, 198405091017),
(2002, '200102151984', 1962, 1904, 908, 20110608, 120207120822, 19850115, 197509241976),
(2003, '196809020302', 2010, 1909, 909, 19661110, 070720132005, 20050405, 200209250804),
(2004, '196908281998', 2007, 1910, 903, 19671219, 063006280908, 20180831, 199707241975),
(2005, '201511281009', 1999, 1908, 910, 19730810, 020819730623, 20100120, 201802062015),
(2006, '199712132009', 2015, 1906, 907, 19720406, 062705111230, 19681216, 201904291013),
(2007, '201609060102', 1973, 1905, 902, 19880306, 201107190924, 19720622, 198811171983),
(2008, '197303192004', 1994, 1901, 905, 19670913, 202003031990, 19860611, 196709140525),
(2009, '199712170312', 2004, 1902, 901, 20100305, 198101260505, 20090127, 196711081968),
(2010, '196910182000', 1972, 1903, 904, 19720524, 200511232002, 20121225, 201004221202),
(2011, '198605150626', 1970, 1907, 906, 20101207, 199806040813, 20080730, 200803302016);

insert into case_index(id, case_index, title_ru, title_kz, title_en, storage_type, storage_year, note, company_unit_id, nomenclature_id, created_timestamp,
                       created_by, updated_timestamp, updated_by)
values
(2101, '200906130330', 'Kosmos 613', 'Soyuz TM-1', 'Soyuz MS-14', 19, 1984, 'Site 31-6', 903, 2006, 20181102, 101306080820,
 20000902, 121911051128),
(2102, '200010111997', 'Kosmos 638', 'Soyuz TMA-3', 'Soyuz 19', 83, 1988, 'Baikonur cosmodrome', 906, 2009, 20160326, 022012250628,
 20151207, 070703021997),
(2103, '201701060910', 'Kosmos 656', 'Apollo spacecraft', 'Apollo–Soyuz', 61, 1989, 'Earth orbit', 901, 2001,
 19961009, 2018040412021995, 19810416, 199205070814),
(2104, '198604071972', 'Kosmos 670', 'Test project', 'Soyuz TMA-14M', 52, 1978, 'N15000-07', 908, 2003,
 20111128, 199301011983, 19660714, 196808230919),
(2105, '199102280618', 'Kosmos 672', 'Soyuz TMA-16M', 'ISS', 71, 1997, 'Site 1-5', 907, 2007, 20180222, 201910051997,
 20041122, 200405291121),
(2106, '201105261977', 'Kosmos 772', 'Shenzhou', 'Space shuttle', 19, 1962, 'Soviet spacecraft', 904, 2010, 20170905, 196907271970,
 20200716, 199905181213),
(2107, '199108161121', 'Soyuz 20', 'Buran spacecraft', 'Soyuz and Vega', 82, 1978, 'IGLA-system', 902, 2004,
 19680630, 201604191986, 19650714, 198206181211),
(2108, '201808032019', 'Kosmos 869', 'Spaceport', 'CNES', 11, 1965, 'Boris Chertok', 910, 2002, 20051221, 196604060330,
 19860316, 196410071986),
(2109, '196110131119', 'Kosmos 1001', 'European Space Agency', 'ESA', 17, 2001, 'Soyuz 1', 905, 2005,
 19640327, 200804171991, 19690820, 197410240412),
(2110, '196304041979', 'Kosmos 1074', 'Shenzhou-5', 'Soyuz programme', 15, 1974, 'Apollo 1', 909, 2008,
 19911206, 200312251962, 19740409, 200510200104),
(2111, '197601300607', 'Soyuz T-1', 'Soyuz 11A51', 'U15000-05', 8, 1998, 'South Atlantic', 903, 2006, 20060206, 199409212014,
 20060329, 199302101022);

insert into record(id, number, type, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
values
(2201, '201006072017', 'Sputnik', 905, 19931111, 196907150203, 19970220, 198005281983),
(2202, '199011101115', 'Vostok', 907, 19720218, 199304230123, 20120911, 197405101209),
(2203, '197501301977', 'Buran', 908, 19990316, 200205071967, 20081223, 197712111977),
(2204, '201601201016', 'Mars 4NM', 906, 20100414, 197203191114, 20140315, 200506020427),
(2205, '200808141967', 'Mars 5NM', 910, 19990805, 198310221974, 20171112, 198709092006),
(2206, '200603300620', 'Mars 5M', 901, 19880801, 201105240413, 20080520, 201906101979),
(2207, '198207191987', 'Vesta', 909, 19850910, 200806162015, 19670318, 196308200626),
(2208, '200605021228', 'Tsiolkovsky', 903, 20181220, 196503160120, 19800205, 201901290212),
(2209, '199911241973', 'Soyuz', 902, 19630218, 197905012006, 19630408, 197412101967),
(2210, '197612090703', 'L1', 904, 19851014, 197701230105, 20180331, 196402160404),
(2211, '201402111967', 'L3', 905, 20120724, 201907061971, 19641018, 198204132018);

insert into searchkey(id, name, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
values
(2301, 'Zvezda', 902, 19711104, 198211190330, 19880411, 198410101990),
(2302, 'TKS', 908, 19701002, 196204070530, 19690814, 199011151969),
(2303, 'Zarya', 910, 20150910, 199909240725, 19630211, 201406241999),
(2304, 'Almaz', 905, 19770123, 201810291020, 20070210, 200607091986),
(2305, 'Salyut-DOS', 909, 20181125, 201101100822, 19980603, 200708022020),
(2306, 'Mir', 906, 19770721, 201510250708, 19920512, 199702061976),
(2307, 'Mir-2', 904, 19640229, 197312100211, 20011205, 200212291991),
(2308, 'Spiral', 907, 19761028, 197501170920, 19891117, 201302032019),
(2309, 'LKS', 903, 19690129, 198602280419, 20121209, 196510161996),
(2310, 'Buran', 901, 20060918, 199702101969, 20181122, 201406130215),
(2311, 'Sputnik', 902, 20110201, 199211160424, 20110204, 198608191982);

insert into search_key_routing(id, search_key_id, table_name, table_id, type)
values
(2401, 2303, 'Kosmos', 199802271101, 'Mars-6'),
(2402, 2306, 'Luna', 199707231983, 'Mars-7'),
(2403, 2304, 'Lunohod', 198904120707, 'KA'),
(2404, 2308, 'E-8', 197711092005, 'Mars'),
(2405, 2310, 'Venera', 196511270301, 'RN Molniya'),
(2406, 2309, 'Vega', 199508091992, 'Mars-1'),
(2407, 2305, 'Mars', 197307071121, 'RN Proton'),
(2408, 2307, 'Mars-2', 198408301961, 'Mars-2'),
(2409, 2301, 'Mars-3', 201908087277, 'Mars-7'),
(2410, 2302, 'Mars-4', 198401972122, 'ISZ'),
(2411, 2303, 'Mars-5', 198501070221, 'Vostok-2');