--1
drop table if exists authorization_table cascade;
create table authorization_table(
                                    id bigint primary key,
                                    username varchar(255),
                                    email varchar(255),
                                    password varchar(128),
                                    role varchar(255),
                                    forgot_password_key varchar(128),
                                    forgot_password_timestamp bigint,
                                    company_unit_id int
);

--2
drop table if exists request_table cascade;
create table request_table(
                              id bigint primary key,
                              request_user_id bigint,
                              response_user_id bigint,
                              case_index_id bigint,
                              created_type varchar(64),
                              comment varchar(255),
                              status varchar(64),
                              timestamp bigint,
                              sharetimestamp bigint,
                              sharefinish bigint,
                              favorite boolean,
                              update_timestamp bigint,
                              update_by bigint,
                              declinenote varchar(255),
                              company_unit_id bigint,
                              from_request_id bigint
);

--3
drop table if exists share_table cascade;
create table share_table(
                            id bigint primary key,
                            request_id bigint,
                            note varchar(255),
                            sender_id bigint,
                            receiver_id bigint,
                            share_timestamp bigint
);

--4
drop table if exists history_of_request_statuses cascade;
create table history_of_request_statuses(
                                            id bigint primary key,
                                            request_id bigint,
                                            status varchar(64),
                                            created_timestamp bigint,
                                            created_by bigint,
                                            updated_timestamp bigint,
                                            updated_by bigint
);

--5
drop table if exists activity_journal cascade;
create table activity_journal(
                                 id bigint primary key,
                                 type_of_event varchar(128),
                                 object_type varchar(255),
                                 object_id bigint,
                                 created_timestamp bigint,
                                 created_by bigint,
                                 message_level varchar(128),
                                 message varchar(255)
);

--6
drop table if exists notification cascade;
create table notification(
                             id bigint primary key,
                             object_type varchar(128),
                             object_id bigint,
                             company_unit_id bigint,
                             user_id bigint,
                             created_timestamp bigint,
                             veiwed_timestamp bigint,
                             is_viewed boolean,
                             title varchar(255),
                             text_of_notification varchar(255),
                             company_id bigint
);

--7
drop table if exists fond cascade;
create table fond(
                     id bigint primary key,
                     fond_number varchar(128),
                     created_timestamp bigint,
                     create_by bigint,
                     updated_timestamp bigint,
                     updated_by bigint
);

--8
drop table if exists company cascade;
create table company(
                        id bigint primary key,
                        name_ru varchar(128),
                        name_kz varchar(128),
                        name_en varchar(128),
                        bin varchar(32),
                        parent_id bigint,
                        fond_id bigint,
                        created_timestamp bigint,
                        created_by bigint,
                        updated_timestamp bigint,
                        updated_by bigint
);

--9
drop table if exists company_unit cascade;
create table company_unit(
                             id bigint primary key,
                             name_ru varchar(128),
                             name_kz varchar(128),
                             name_en varchar(128),
                             parent_id bigint,
                             year int,
                             company_id bigint,
                             code_index varchar(16),
                             created_timestamp bigint,
                             created_by bigint,
                             updated_timestamp bigint,
                             updated_by bigint
);

--10
drop table if exists users cascade;
create table users(
                      id bigint primary key,
                      auth_id bigint,
                      name varchar(128),
                      fullname varchar(128),
                      surname varchar(128),
                      secondname varchar(128),
                      status varchar(128),
                      company_unit_id bigint,
                      password varchar(128),
                      last_login_timestamp bigint,
                      iin varchar(32),
                      is_active boolean,
                      is_activated boolean,
                      created_timestamp bigint,
                      created_by bigint,
                      updated_timestamp bigint,
                      updated_by bigint
);

--11
drop table if exists catalog cascade;
create table catalog(
                        id bigint primary key,
                        name_ru varchar(128),
                        name_kz varchar(128),
                        name_en varchar(128),
                        parent_id bigint,
                        company_unit_id bigint,
                        created_timestamp bigint,
                        created_by bigint,
                        updated_timestammp bigint,
                        updated_by bigint
);

--12
drop table if exists catalog_case cascade;
create table catalog_case(
                             id bigint primary key,
                             case_id bigint,
                             catalog_id bigint,
                             company_unit_id bigint,
                             created_timestamp bigint,
                             created_by bigint,
                             updated_timestamp bigint,
                             updated_by bigint
);

--13
drop table if exists location cascade;
create table location(
                         id bigint primary key,
                         row varchar(64),
                         line varchar(64),
                         column_location varchar(64),
                         box varchar(64),
                         company_unit_id bigint,
                         created_timestamp bigint,
                         created_by bigint,
                         updated_timestamp bigint,
                         updated_by bigint
);

--14
drop table if exists tempfiles cascade;
create table tempfiles(
                          id bigint primary key,
                          file_binary text,
                          file_binary_byte bytea
);

--15
drop table if exists file cascade;
create table file(
                     id bigint primary key,
                     name varchar(128),
                     type varchar(128),
                     size bigint,
                     page_count int,
                     hash varchar(128),
                     is_deleted boolean,
                     file_binary_id bigint,
                     created_timestamp bigint,
                     created_by bigint,
                     updated_timestamp bigint,
                     updated_by bigint
);

--16
drop table if exists case_table cascade;
create table case_table(
                           id bigint primary key,
                           case_number varchar(128),
                           folder_number varchar(128),
                           case_title_ru varchar(128),
                           case_title_kz varchar(128),
                           case_title_en varchar(128),
                           start_date bigint,
                           end_date bigint,
                           pages_amount bigint,
                           ecp_flag boolean,
                           ecp_text text,
                           naf_send boolean,
                           deletion_flag boolean,
                           restricted_access_flag boolean,
                           hash varchar(128),
                           version int,
                           id_version varchar(128),
                           is_active_flag boolean,
                           comment varchar(255),
                           id_location bigint,
                           id_index bigint,
                           id_deletion_document bigint,
                           structure_unit_id bigint,
                           blockchain_address varchar(128),
                           adding_to_blockchain_date bigint,
                           creating_date bigint,
                           case_author bigint,
                           case_editing bigint,
                           case_editing_author bigint
);

--17
drop table if exists file_routing cascade;
create table file_routing(
                             id bigint primary key,
                             file_id bigint,
                             table_name varchar(128),
                             table_id bigint,
                             type varchar(128)
);

--18
drop table if exists delete_document cascade;
create table delete_document(
                                id bigint primary key,
                                document_number varchar(128),
                                reason varchar(128),
                                company_unit_id bigint,
                                creation_date bigint,
                                author bigint,
                                edition_date bigint,
                                edition_author bigint
);

--19
drop table if exists nomenclature_summary cascade;
create table nomenclature_summary(
                                     id bigint primary key,
                                     number_of_nomenclature_summary varchar(128),
                                     year int,
                                     company_unit_id bigint,
                                     created_timestamp bigint,
                                     created_by bigint,
                                     updated_timestamp bigint,
                                     updated_by bigint
);

--20
drop table if exists nomenclature cascade;
create table nomenclature(
                             id bigint primary key,
                             nomenclature_number varchar(128),
                             year int,
                             nomenclature_summary_id bigint,
                             company_unit_id bigint,
                             created_timestamp bigint,
                             created_by bigint,
                             updated_timestamp bigint,
                             updated_by bigint
);

--21
drop table if exists case_index cascade;
create table case_index(
                           id bigint primary key,
                           case_index varchar(128),
                           title_ru varchar(128),
                           title_kz varchar(128),
                           title_en varchar(128),
                           storage_type int,
                           storage_year int,
                           note varchar(128),
                           company_unit_id bigint,
                           nomenclature_id bigint,
                           created_timestamp bigint,
                           created_by bigint,
                           updated_timestamp bigint,
                           updated_by bigint
);

--22
drop table if exists record cascade;
create table record(
                       id bigint primary key,
                       number varchar(128),
                       type varchar(128),
                       company_unit_id bigint,
                       created_timestamp bigint,
                       created_by bigint,
                       updated_timestamp bigint,
                       updated_by bigint
);

--23
drop table if exists searchkey cascade;
create table searchkey(
                          id bigint primary key,
                          name varchar(128),
                          company_unit_id bigint,
                          created_timestamp bigint,
                          created_by bigint,
                          updated_timestamp bigint,
                          updated_by bigint
);

--24
drop table if exists search_key_routing cascade;
create table search_key_routing(
                                   id bigint primary key,
                                   search_key_id bigint,
                                   table_name varchar(128),
                                   table_id bigint,
                                   type varchar(128)
);

alter table share_table
    add constraint request_id_fk
        foreign key (request_id)
            references request_table(id);

alter table history_of_request_statuses
    add constraint request_id_fk_h_o_r_s
        foreign key (request_id)
            references request_table(id);

alter table company
    add constraint fond_id_fk_company
        foreign key (fond_id)
            references fond(id);


alter table company_unit
    add constraint company_id_fk_company_unit
        foreign key (company_id)
            references company(id);


alter table users
    add constraint company_unit_id_fk_from_company_unnit
        foreign key (company_unit_id)
            references company_unit(id);

alter table catalog
    add constraint company_unit_fk_catalog
        foreign key (company_unit_id)
            references company_unit(id);

alter table catalog_case
    add constraint catalog_id_fk_from_catalog
        foreign key (catalog_id)
            references catalog(id);

alter table catalog_case
    add constraint company_unit_id_fk_calatog_case
        foreign key (company_unit_id)
            references company_unit(id);

alter table location
    add constraint company_unit_id_fk_location
        foreign key (company_unit_id)
            references company_unit(id);

alter table file
    add constraint file_binary_id_fk
        foreign key (file_binary_id)
            references tempfiles(id);

alter table case_table
    add constraint id_location_fk
        foreign key (id_location)
            references location(id);

alter table case_table
    add constraint structure_unit_id_fk_case_table
        foreign key (structure_unit_id)
            references company_unit(id);

alter table file_routing
    add constraint file_id_fk_file_routing
        foreign key (file_id)
            references file(id);

alter table delete_document
    add constraint company_unit_id_delete_document
        foreign key (company_unit_id)
            references company_unit(id);

alter table nomenclature_summary
    add constraint company_unit_id_fk_nomenclature_summary
        foreign key (company_unit_id)
            references company_unit(id);

alter table nomenclature
    add constraint nomenclature_summary_id_fk
        foreign key (nomenclature_summary_id)
            references nomenclature_summary(id);

alter table nomenclature
    add constraint company_unit_id_nomenclature
        foreign key (company_unit_id)
            references company_unit(id);

alter table case_index
    add constraint company_unit_id_fk_case_index
        foreign key (company_unit_id)
            references company_unit(id);

alter table case_index
    add constraint nomenclature_id_fk_case_index
        foreign key (nomenclature_id)
            references nomenclature(id);

alter table record
    add constraint company_unit_id_fk_record
        foreign key (company_unit_id)
            references company_unit(id);

alter table searchkey
    add constraint company_unit_id_fk_searchkey
        foreign key (company_unit_id)
            references company_unit(id);

alter table search_key_routing
    add constraint search_key_id_fk
        foreign key (search_key_id)
            references searchkey(id);